"use strict";

var MAILTRAP_API = "454e0809a914b934cd31faa4125b3627";
var MAILTRAP_INBOX = "Demo inbox";

var request = require('request');

var baseURL = "https://mailtrap.io/api/v1/";
var headers = {
	"Content-Type": "application/json",
	"Api-Token": MAILTRAP_API
};

function getMessages(inboxID, email, done) {
	inboxID = inboxID || MAILTRAP_INBOX;

	var options = {
		method: "GET",
		url: baseURL + "/inboxes/" + inboxID + "/messages",
		headers: headers
	};

	request(options, function (error, response, body) {
		if (error)
			return done(error);

		if (response.statusCode >= 400)
			return done("Response error:" + response.statusCode + " " + response.statusMessage);

		var messages = [];

		var result = JSON.parse(body);

		result.forEach(function(msg) {
			if (email == null || msg.to_email == email)
				messages.push(msg);
		});

		done(null, messages);
	});
}

function getTokenFromMessage(email, re, done) {
	getMessages(null, email, function(err, messages) {
		if (err) 
			return done(err);

		if (messages.length < 1)
			return done("Passwordless email not received!");
		
		let msg = messages[0];
		// Get the last email body
		let body = msg.html_body;

		let match = re.exec(body);
		if (match)
			return done(null, match[1], msg);

		return done("Token missing from email! " + body);
	});
}

function cleanInbox(inboxID, done) {
	inboxID = inboxID || MAILTRAP_INBOX;
	if (!done)
		done = function(err) {
			if (err)
				console.error(err);
		};

	var options = {
		method: "PATCH",
		url: baseURL + "/inboxes/" + inboxID + "/clean",
		headers: headers
	};

	request(options, function (error, response, body) {
		if (error)
			return done(error);

		if (response.statusCode >= 400)
			return done("Response error:" + response.statusCode + " " + response.statusMessage);

		done();
	});
}

function deleteMessage(inboxID, messageID, done) {
	inboxID = inboxID || MAILTRAP_INBOX;
	if (!done)
		done = function(err) {
			if (err)
				console.error(err);
		};

	var options = {
		method: "DELETE",
		url: baseURL + "/inboxes/" + inboxID + "/messages/" + messageID,
		headers: headers
	};

	request(options, function (error, response, body) {
		if (error)
			return done(error);

		if (response.statusCode >= 400)
			return done("Response error:" + response.statusCode + " " + response.statusMessage);

		done();
	});
}

module.exports = {
	getMessages,
	getTokenFromMessage,
	cleanInbox,
	deleteMessage
}