/// <reference types="cypress" />
import Chance from 'chance';
const chance = new Chance();

describe('order test', () => {


    beforeEach(() => {
        cy.visit('https://ledlegtesting.firebaseapp.com/login');
        cy.clearCookies()
        cy.login('gabi.harangozo29@hotmail.com','4c37a8ed');
        cy.wait(1000)
    })

    afterEach(() => {
     
    })

    it('buy the first product', () => {
        cy.get("button[name='addToBasket']").first().click();
        cy.get("button[routerlink='/order']").first().click();
        
        cy.get('.waves-effect:nth-child(2)').first().should('contain',1);
        cy.get('h3').first().should('contain','100,001 Ft');
        cy.get('.btn:nth-child(3)').first().click(  {force: true} );
        cy.wait(1000)
        cy.visit('https://ledlegtesting.firebaseapp.com/order');
        cy.get('.waves-effect:nth-child(2)').first().should('contain',2);
        cy.get('h3').first().should('contain','200,002 Ft');
        cy.get('button[type="button"]').first().click(  {force: true} );
        cy.wait(1000)
        cy.get('.submitButton > .btn').first().click(  {force: true} );
        cy.wait(1000)
        cy.get('#cdk-step-content-0-2 .btn').first().click(  {force: true} );
        cy.wait(1000)
        cy.get('#cdk-step-content-0-3 span:nth-child(5) > h4').first().should('contain','201,302 Ft');
        cy.get('span > .btn').first().click(  {force: true} );
        cy.get('#mat-dialog-title-0').first().should('contain','Sikeres Rendelés!');

       
        
    });


});