/// <reference types="cypress" />
import Chance from 'chance';
const chance = new Chance();
const apiKey = ''

describe('Login and register test', () => {

    const email = chance.email();
    const pass = 'randomPassword123';
    const mailtrap = require("../util/mailtraphelper");

    beforeEach(() => {
        cy.visit('https://ledlegtesting.firebaseapp.com/login');
    })

    /*it('Register the user', () => {
        cy.visit("https://ledlegtesting.firebaseapp.com/register");
        cy.get("input[type='email']").type(email);
        cy.get("#password1").type(pass);
        cy.get("#password2").type(pass);
        cy.get("button[type='submit']").click();		
        mailtrap.getMessages(null,email,(err, message)=> {
            if (err) 
                    throw new Error(err);

            alert(message)
            console.log( message)
        });
    });*/
    it('it should login with the right certs', () => {

        // Assert URL
        cy.url().should('include', 'login');

        // Fill out the form
        cy.login('gabi.harangozo29@hotmail.com', '4c37a8ed');
        cy.url().should('include', 'home');
        cy.get('app-user-menu').contains('Harangozó Gábor Vevő').should(() => {
            expect(localStorage.getItem('email')).to.eq('gabi.harangozo29@hotmail.com');
            expect(localStorage.getItem('id_token')).to.not.equal(null)
        });
        cy.logout();
    });

    it('it should not login with the not right certs', () => {

        // Assert URL
        cy.url().should('include', 'login');
        // Fill out the form
        cy.login(email, pass);
        // Assert welcome message
        cy.get('snack-bar-container').contains('Nincs ilyen felhasználó!');
    });


    it('it should log out', () => {
        cy.login('gabi.harangozo29@hotmail.com', '4c37a8ed');
        cy.logout();
        cy.url().should('include', 'home');
        cy.get('app-user-menu').contains('Bejelentkezés').should(() => {
            expect(localStorage.getItem('email')).to.null;
            expect(localStorage.getItem('id_token')).to.null;
        });
    });

});