/// <reference types="cypress" />
import Chance from 'chance';
const chance = new Chance();

describe('product test', () => {


    beforeEach(() => {
        cy.visit('https://ledlegtesting.firebaseapp.com/home');
        cy.login('gabi.harangozo29@hotmail.com','4c37a8ed');
        cy.wait(1000)
    })

    afterEach(() => {
        cy.logout();
    })

    /*it('should filter by price', () => {
        cy.url().should('include', 'home');
        cy.get('mat-expansion-panel').click();
        cy.get('.min').clear();
        cy.get('.min').type(100000);
        cy.wait(10000);
        cy.log(cy.get('.ng-star-inserted:nth-child(1) .card-body strong').eq(0).invoke('text'))
        //assert.isAbove(,100000)
        
    });*/
    
    it('should filter by category', () => {
        cy.get('app-desktop-menu').click();
        cy.get("button[mat-menu-item]").eq(1).click();
        cy.wait(1000);
        let someText;
        cy.get(".subItem").first().invoke('text')
            .then(text => {
                someText = text;
                cy.log(someText)
                cy.get('.subItem').first().click();
                cy.get('h5').first().should('have.text', someText)
            });

    });

    it('write an oppinion for the first product', () => {
        cy.wait(1000);
        cy.get('mat-card').first().click();
        cy.url().should('include','product/019a39cc-7184-4572-8a26-b74540e54762');
        cy.get("#opinionText").type("ok");
        cy.get('button[type=submit]').first().click();
        cy.get('.rate').contains('Harangozó Gábor Vevő');
        cy.get('.rate').contains('ok');
        cy.get('snack-bar-container').contains('Köszönjük a vélemény nyilvánítást!');
    });

    it('should search and open a product', () => {
        cy.get('input[placeholder="Keress a termékek között"]').type('valami');
        cy.get('.mat-option-text').first().click();
        cy.get('.wrapper').contains('valami');

    });




});