export const environment = {
  production: true,
  firebase : {
    apiKey: "AIzaSyB4skA2jVLKg-P_RXpWMXtTVq-YinOPH5c",
    authDomain: "ledleg-41cfc.firebaseapp.com",
    databaseURL: "https://ledleg-41cfc.firebaseio.com",
    projectId: "ledleg-41cfc",
    storageBucket: "ledleg-41cfc.appspot.com",
    messagingSenderId: "992742618101"
  },/*
  URLDownload : 'http://db.presentforyou.hu/api/Containers/container1/download/',
  URLDelete: 'http://db.presentforyou.hu/api/Containers/container1/files/',
  URLUpload: 'http://db.presentforyou.hu/api/Containers/container1/upload/',
  loopbackUrl: 'http://db.presentforyou.hu'*/
  URLDownload : 'https://ledlegloopback.herokuapp.com/api/Containers/container1/download/',
  URLDelete: 'https://ledlegloopback.herokuapp.com/api/Containers/container1/files/',
  URLUpload: 'https://ledlegloopback.herokuapp.com/api/Containers/container1/upload/',
  loopbackUrl: 'https://ledlegloopback.herokuapp.com'
};