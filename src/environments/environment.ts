// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase : {
    apiKey: "AIzaSyB4skA2jVLKg-P_RXpWMXtTVq-YinOPH5c",
    authDomain: "ledleg-41cfc.firebaseapp.com",
    databaseURL: "https://ledleg-41cfc.firebaseio.com",
    projectId: "ledleg-41cfc",
    storageBucket: "ledleg-41cfc.appspot.com",
    messagingSenderId: "992742618101"
  },
  URLDownload : 'http://localhost:3000/api/Containers/container1/download/',
  URLDelete: 'http://localhost:3000/api/Containers/container1/files/',
  URLUpload: 'http://localhost:3000/api/Containers/container1/upload/',
  loopbackUrl: 'http://localhost:3000'/*
  URLDownload : 'https://ledlegloopback.herokuapp.com/api/Containers/container1/download/',
  URLDelete: 'https://ledlegloopback.herokuapp.com/api/Containers/container1/files/',
  URLUpload: 'https://ledlegloopback.herokuapp.com/api/Containers/container1/upload/',
  loopbackUrl: 'https://ledlegloopback.herokuapp.com'*/
}

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI
