import { SubCategory } from "./sub-category";

export interface Product {
    id: string,
    subCategory: SubCategory,
    name: String,
    about: String,
    small_about: String,
    pictures: String[],
    price: Number,
    dealer: String,
    available: Number
}
