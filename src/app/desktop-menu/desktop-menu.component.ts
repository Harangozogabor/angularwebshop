import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-desktop-menu',
  templateUrl: './desktop-menu.component.html',
  styleUrls: ['./desktop-menu.component.scss']
})
export class DesktopMenuComponent implements OnInit {
  isOpen = false;
  isOpen2 = false;
  @Input() categories;
  subcategories=[];
  constructor(private _dataService:DataService, private router:Router) { }

  ngOnInit() {
    setTimeout(() => {
      this.categories.forEach(element => {
        this._dataService.getSubCategoriesByMainCategoryId(element.id).subscribe(data=>{
          this.subcategories[element.id]=data;
        },
        error=>{
          console.error(error)
        })
      });
    }, 0);
  }
  mouseLeftSub() {
    this.isOpen2 = false;
  }
  goProduct(mainCategory,subcategory){
    this.router.navigate(['/products'], { queryParams: { mainCategory: mainCategory.id,subCategory:subcategory ? subcategory.id : null },queryParamsHandling: 'merge' });
  }

}
