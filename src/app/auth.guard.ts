import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot} from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from './services/auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { DataService } from './services/data.service';
import { environment } from 'src/environments/environment';
import { Http } from '@angular/http';
import { map, first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService,private dataService:DataService, private router: Router,private afAuth: AngularFireAuth,private http:Http) {}

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot){
    let url = environment.loopbackUrl + '/api/People/getPerson?email=' + localStorage.getItem('email');
    return this.http.get(url).pipe(
      map((user:any)=> JSON.parse(user._body)[0].role==='admin'),
      first()
      )
   }
}
