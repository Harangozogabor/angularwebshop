import { ErrorHandler, Injector } from "@angular/core";
import { Http } from '@angular/http';
import { environment } from "src/environments/environment";
import { AuthService } from "./services/auth.service";
declare function require(name: string);
const uuidv1 = require('uuid/v1');
export class MyErrorHandler implements ErrorHandler {
    auth;
    http;
    constructor(private injector: Injector) {
        setTimeout(() => {
            this.auth = this.injector.get(AuthService);
            this.http = this.injector.get(Http);
        }, 0);
    }
    handleError(error) {
        console.error(error)
        const errorObject = { id: uuidv1(), date: new Date(), bugMaker: this.auth ? this.auth.getUserEmail() : null, bug: error.message }
        console.log(errorObject)
        this.http.post(environment.loopbackUrl + '/api/errors/addError', errorObject)
        .subscribe(()=>{

        },error=>{
            console.error(error)
        })
    }
}
