import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { switchMap, filter } from 'rxjs/operators';
import { Order } from '../shared/sdk';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  displayedColumns: string[] = ['edit', 'icon', 'id', 'username', 'date', 'status'];
  dataSource: MatTableDataSource<any>;
  filterValue='';
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  adminStuffs;
  fromDate= new Date(0);
  toDate = new Date();
  constructor(private _dataService: DataService, public dialog: MatDialog) {
    this._dataService.getOrders().subscribe(data => {
      for(let i =0; i<data.length;i++){
        data[i].username=data[i].user.lastName+' '+data[i].user.firstName;
      }
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.sortingDataAccessor = (item, property) => {
        switch (property) {
           case 'date': return new Date(item.order_date);
           default: return item[property];
        }
      };
      this.dataSource.filterPredicate = (data, filter: string)=> {
        let filterValue = false;
        filterValue = data.user.lastName.trim().toLowerCase().includes(filter) || data.user.firstName.trim().toLowerCase().includes(filter) || data.id.trim().toLowerCase().includes(filter);
        return (new Date(data.order_date).getTime() > this.fromDate.getTime() && new Date(data.order_date).getTime() < this.toDate.getTime()) 
          && filterValue;
      }
    })
  }

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  openDialog(order): void {
    const dialogRef = this.dialog.open(EditorDialog, {
      width: '80%',
      data: { order: order }
    });

    dialogRef.afterClosed().pipe(switchMap(()=>this._dataService.getOrders())).subscribe(data => {
      for(let i =0; i<data.length;i++){
        data[i].username=data[i].user.name;
      }
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  toDateChanged(e){
    this.applyFilter(this.filterValue)
  }

  fromDateChanged(e){
    this.applyFilter(this.filterValue)
  }


}


@Component({
  selector: 'editorDialog',
  templateUrl: 'orderEditorModal.html',
})
export class EditorDialog {
  status:string;
  date = new Date();
  minDate= new Date();
  constructor(
    public dialogRef: MatDialogRef<EditorDialog>, @Inject(MAT_DIALOG_DATA) public data, private router: Router,private dataService:DataService) {
      this.status=data.order.status;
      this.date= new Date(data.order.expected_arrive_date);
      
     }

  save(): void {
    if(this.status!=this.data.order.status ){
      this.dataService.updateOrder(this.data.order.id,this.status).then();
    }
    if(new Date(this.date).toISOString()!=this.data.expected_arrive_date){
      this.dataService.updateOrder(this.data.order.id,null,new Date(this.date).toISOString()).then();
    }
    this.dialogRef.close();
  }
  cancel(): void {
    this.dialogRef.close();
  }
}
