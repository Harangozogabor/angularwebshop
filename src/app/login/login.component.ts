import { Component, OnInit, Input, Inject } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { DataService } from '../services/data.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { MatDialogRef, MatDialog } from '@angular/material';
import { MatSnackBar } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  @Input() shouldRoute = true;
  margin = 'extra'
  isLocalStorage = false;
  loginUserData = {
    email: '',
    password: '',
  };

  constructor(private _auth: AuthService, public dialog: MatDialog, public snackbar: MatSnackBar, private translate: TranslateService,
    private _router: Router, private dataService: DataService, private afAuth: AngularFireAuth, private ngxService: NgxUiLoaderService) { }

  ngOnInit() {
    if (!this.shouldRoute) {
      this.margin = 'no';
    }
  }

  login() {
    this.ngxService.start();
    this._auth.signInRegular(this.loginUserData.email, this.loginUserData.password).then((data) => {
      this._auth.getToken(this.loginUserData.email)
      this._auth.endOfAuthObserver.subscribe(() => {
        console.log("vége az authnak")
        this.saveUser();
      });
    }).catch(error => {
      console.error(error.code)
      this.translate.get(error.code, { value: '' }).subscribe((res: string) => {
        this.snackbar.open(res, null, {
          duration: 3000,
        });
      });
    });


  }

  async loginWithFacebook() {
    let resp = await this._auth.signInWithFacebook();
    this.isLocalStorage = true;
    this.saveUser();
  }

  async loginWithGoogle() {
    let resp = await this._auth.signInWithGoogle();
    this.isLocalStorage = true;
    this.saveUser();
  }

  saveUser() {
    this.ngxService.start();
    console.log(this._auth.getUser())
    if (this._auth.getUser()!==null) {
      this.dataService.getUserByEmail(this._auth.getUser().email || localStorage.getItem('email')).then(user => {  
        this.ngxService.stop();
        if (user.length != 0) {
          this._auth.observableUser.next(user[0]);
          if (this.isLocalStorage) {
            localStorage.setItem('email', user[0].email);
          }
          console.log(user)
          if (this.shouldRoute && user[0].role !== 'admin') {
            this._router.navigateByUrl("/")
          }
        } else {
          this.ngxService.stop();
          localStorage.setItem('email', this._auth.getUser().email);
          this._auth.observableUser.next({
            lastName: this._auth.getUser().displayName ? this._auth.getUser().displayName.split(" ")[1] : '',
            firstName: this._auth.getUser().displayName ? this._auth.getUser().displayName.split(" ")[0] : ''
          });
          console.log("naviagte")
          this._router.navigate(["/userDetails", 1])
        }
      }, error => {
        console.error(error)
        this.ngxService.stop();
      }).catch(error => {
        console.error(error)
        this.ngxService.stop();
      })
    } else {
      this.ngxService.stop();
    }

  }

  openModal() {
    const dialogRef = this.dialog.open(PasswordResetDialog, {
      width: '250px'
    });
  }

}
@Component({
  selector: 'passwordReset',
  templateUrl: 'passwordReset.html',
})
export class PasswordResetDialog {

  constructor(
    public dialogRef: MatDialogRef<PasswordResetDialog>, private auth: AuthService, private snackbar: MatSnackBar) {
  }

  public email;
  onNoClick(): void {
    this.dialogRef.close();
  }
  onYesClick(): void {
    this.auth.resetPassword(this.email).then(data => {
      this.dialogRef.close();
      this.snackbar.open("A megadott email címre elküldtünk egy emailt amiben megváltoztathatod a jelszavad", null, {
        duration: 3000,
      });
    }, error => {
      console.error(error)
      this.snackbar.open(error, null, {
        duration: 3000,
      });
    })
  }

}