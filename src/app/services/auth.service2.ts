import { Injectable, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { DataService } from './data.service';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { SDKToken, LoopBackConfig } from '../shared/sdk';
import { LoopBackAuth } from '../shared/sdk/services/core/auth.service';
import { isDefaultChangeDetectionStrategy } from '@angular/core/src/change_detection/constants';
declare function require(name: string);
const uuidv1 = require('uuid/v1');
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Injectable({
  providedIn: 'root'
})
export class AuthService implements OnInit {
  private regist = false;
  private storedName;
  private refreshtimes = 0;
  private facebookLogin = false;
  private phoneLogin = false;
  private fulfilledAdmin = false;
  private login = false;
  private storedEmail;
  private dataBaseUser;
  private user: Observable<firebase.User>;
  private userDetails: firebase.User = null;
  observableUser = new BehaviorSubject<any>(this.dataBaseUser);
  cast = this.observableUser.asObservable();
  public filledProfile = false;
  observableFilledProfile = new BehaviorSubject<boolean>(this.filledProfile);
  castFilled = this.observableFilledProfile.asObservable();
  changedUser = new BehaviorSubject<any>('');
  castChangedUser = this.changedUser.asObservable();
  endOfAuth = new BehaviorSubject<any>('');
  endOfAuthObserver = this.endOfAuth.asObservable();
  constructor(private _firebaseAuth: AngularFireAuth, private _router: Router, private dataService: DataService, private http: HttpClient, private auth: LoopBackAuth) {
  
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.userDetails = user;
        if (this.login) {
          if (user.emailVerified || this.facebookLogin || this.phoneLogin) {
            this.getToken(user.email ? user.email : localStorage.getItem('email'));
            this.login = !this.login;
          } else {
            if (this.login) {
              this.logout();
              alert("please verify email")
            }
          }
        }
        if (!this.regist) {
          this.dataService.getUserByEmail(user.email ? user.email : localStorage.getItem('email')).then(data => {
            if (data[0]) {
              this.observableUser.next({ firstName: data[0].firstName ? data[0].firstName : 'Felhasználó', lastName: data[0].lastName ? data[0].lastName : '' })
              if (data[0].billingCity != '') {
                this.filledProfile = true;
              } else {
                this.filledProfile = false;
              }
              this.observableFilledProfile.next(this.filledProfile);
            } else {
              this.observableUser.next({ firstName: user.displayName ? user.displayName.split(" ")[0] : 'Felhasználó', lastName: user.displayName ? user.displayName.split(" ")[1] : '' })
            }
          }, err => {
            console.error(err)
          });
        }
      } else {
        this.userDetails = null;
      }
      if (!this.login) {
        this.tokenrefresh();
      }
    })
  }

  ngOnInit() {

  }
  resetPassword(email) {
    return firebase.auth().sendPasswordResetEmail(email);
  }
  tokenrefresh() {
    if (firebase.auth().currentUser !== null) {
      firebase.auth().currentUser.getIdTokenResult().then(data => {
        let now = new Date(new Date().setMinutes(new Date().getMinutes() + 5)).getTime();
        if (new Date(data.expirationTime).getTime() > now && localStorage.getItem('id_token') !== null && localStorage.getItem('id_token')===data.token) {
          setTimeout(() => {
            this.tokenrefresh();
          }, 5 * 60 * 1000);
        } else {
          if (localStorage.getItem('id_token')) {
            firebase.auth().currentUser.getIdToken(true)
              .then((idToken: any) => {
                let httpOptions = {
                  headers: new HttpHeaders({ 'Content-Type': 'application/json', 'Authorization': localStorage.getItem('id_token') })
                };
                this.http.post(environment.loopbackUrl + "/api/CustomTokens", { id: uuidv1(), ttl: 1209600, created: new Date().toISOString(), userId: data.claims.user_id, firebaseToken: idToken, dontsave: true }, httpOptions).subscribe(() => {
                  localStorage.setItem('id_token', idToken);
                  let token = new SDKToken();
                  token.id = idToken;
                  this.auth.setToken(token);
                }, error => {
                  console.error(error)
                })
                setTimeout(() => {
                  this.tokenrefresh();
                }, 60 * 55 * 1000);
              }).catch(function (error) {
                console.log(error);
                this.logout();
                setTimeout(() => {
                  this.tokenrefresh();
                }, 60 * 55 * 1000);
              });
          } 
        }

      })
      return;
    } else {
      this.refreshtimes++;
      if (this.refreshtimes < 10) {
        setTimeout(() => {
          this.tokenrefresh();
        }, 500);
      }
      return;
    }

  }
  signInWithFacebook() {
    this.facebookLogin = true;
    this.login = true;
    this.changedUser.next('');
    return this._firebaseAuth.auth.signInWithPopup(
      new firebase.auth.FacebookAuthProvider()
    )
  }

  signInWithGoogle() {
    this.facebookLogin = false;
    this.login = true;
    this.changedUser.next('');
    return this._firebaseAuth.auth.signInWithPopup(
      new firebase.auth.GoogleAuthProvider()
    )
  }
  getUserData() {
    this.dataBaseUser = this.dataService.getUserByEmail(this.getUserEmail());
    return this.dataBaseUser;
  }

  async loginWithPhone(appVerifier) {
    this.login = true;
    this.phoneLogin = true;
    let user = await this.dataService.getUserByEmail(localStorage.getItem('email'));
    this.storedName = user[0] ? user[0].lastName + ' ' + user[0].firstName : '';
    return this._firebaseAuth.auth.signInWithPhoneNumber(user[0].phone, appVerifier);
  }

  loginAdmin() {
    let authService = this;
    let token = new SDKToken();
    firebase.auth().currentUser.getIdTokenResult(true).then(function (idToken) {
      authService.http.post(environment.loopbackUrl + "/api/CustomUsers/add_to_login", { "email": localStorage.getItem('email'), "password": localStorage.getItem('email'), "firebaseToken": idToken.token }, httpOptions).subscribe((data2: any) => {//exist, so log in.
        if (!localStorage.getItem('id_token')) {
          localStorage.setItem('id_token', idToken.token);
          token.id = idToken.token;
          authService.auth.setToken(token);
        }
        authService.changedUser.next('');
      });
    }).catch(function (error) {
      console.error(error)
    });
  }

  sendEmailVerification() {
    return firebase.auth().currentUser.sendEmailVerification();
  }

  getUser() {
    return this.userDetails;
  }
  signInRegular(email, password) {
    this.regist = false;
    this.facebookLogin = false;
    this.login = true;
    this.changedUser.next('');
    return this._firebaseAuth.auth.signInWithEmailAndPassword(email, password)
  }

  register(email, password) {
    this.login = false;
    this.regist = true;
    return this._firebaseAuth.auth.createUserWithEmailAndPassword(email, password);
  }


  isLoggedIn() {
    if (this.userDetails == null) {
      return false;
    } else {
      return true;
    }
  }


  logout() {
    console.log("LOGOUT");
    localStorage.removeItem("id_token");
    this.auth.clear();
    this.http.post(environment.loopbackUrl + "/api/CustomUsers/logout", '', httpOptions);
    sessionStorage.removeItem('email');
    localStorage.removeItem('email');
    this._firebaseAuth.auth.signOut()
      .then((res) => {
        this.observableUser.next({ firstName: '', lastName: '' })
        this._router.navigate(['/']);
      });
  }

  getUserEmail() {
    if (localStorage.getItem('email')) {
      return localStorage.getItem('email')
    } else {
      return null;
    }

  }

  isAdmin() {
    let url = environment.loopbackUrl + '/api/People/getPerson?email=' + localStorage.getItem('email');
    this.http.get(url).subscribe((user: any) => {//check if user is exist
      if (user.length != 0) {
        this.http.get(environment.loopbackUrl + '/api/People/isAdmin?id=' + user[0].id).subscribe(isAdmin => {//check if admin
          return isAdmin;
        }, error => {
          console.error(error);
          return false;
        })
      }
    }, error => {
      console.error(error);
      return false;
    });

  }

  isUser() {
    let url = environment.loopbackUrl + '/api/People/getPerson?email=' + localStorage.getItem('email');
    this.http.get(url).subscribe((user: any) => {//check if user is exist
      if (user.length != 0) {
        this.http.get(environment.loopbackUrl + '/api/People/isAdmin?id=' + user[0].id).subscribe(isAdmin => {//check if admin
          return !isAdmin;
        }, error => {
          console.error(error)
        })
      }
    }, error => {
      console.error(error)
    });
  }

  getToken(email) {
    if (email) {
      localStorage.setItem('email', email);
      this.storedEmail = email;
    }
    let token = new SDKToken();
    let url = environment.loopbackUrl + '/api/CustomUsers/find_one?email=' + email;
    firebase.auth().currentUser.getIdTokenResult(true).then((idToken) => {
      this.http.get(url).subscribe((user: any) => {//check if user is exist
        if (user.length != 0) {
          this.http.get(environment.loopbackUrl + '/api/People/getPerson?email=' + email).subscribe(data => {
            this.http.get(environment.loopbackUrl + '/api/People/isAdmin?id=' + data[0].id).subscribe(isAdmin => {//check if admin
              if (isAdmin) {
                localStorage.setItem('id_token', idToken.token);
                token.id = idToken.token;
                this.auth.setToken(token);
                if (!this.phoneLogin) {
                  this._router.navigateByUrl('adminLogin');
                } else {
                  this.phoneLogin = false;
                }
                this.endOfAuth.next('');
              } else {
                this.http.post(environment.loopbackUrl + "/api/CustomUsers/add_to_login", { "email": email, "password": email, "firebaseToken": idToken.token }, httpOptions).subscribe((data2: any) => {//exist, so log in.
                  if (!localStorage.getItem('id_token')) {
                    localStorage.setItem('id_token', idToken.token);
                    token.id = idToken.token;
                    this.auth.setToken(token);
                  }
                  this.endOfAuth.next('');
                });
              }
            })
          },error=>{
            console.error(error);
          });
        } else {
          if (email) {
            this.http.post(environment.loopbackUrl + "/api/CustomUsers", { "email": email, "password": email }, httpOptions).subscribe(data => {//not exist, register it
              this.http.post(environment.loopbackUrl + "/api/CustomUsers/add_to_login", { "email": email, "password": email, "firebaseToken": idToken.token }, httpOptions).subscribe((data2: any) => {// log it.
                if (!localStorage.getItem('id_token')) {
                  localStorage.setItem('id_token', idToken.token);
                  token.id = idToken.token;
                  this.auth.setToken(token);
                }
                this.endOfAuth.next('');
              });
            }, error => {
              console.error(error);
            })
          }
        }
      }, error => {
        console.log(error)
      })
    }, error => {
      console.error(error)
    });
  }


}
