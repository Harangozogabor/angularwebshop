import { Injectable } from '@angular/core';
import { DataService } from './data.service';
import { CookieService } from 'ngx-cookie-service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {
  cartLength = 0;
  observableCartLength = new BehaviorSubject<number>(this.cartLength);
  cast = this.observableCartLength.asObservable();
  constructor(private _dataService: DataService, private cookie: CookieService) {
    this.getCartContent().then(data => {
      if (data.length != 0) {
        this.cartLength = data[0].products.length;
        this.eventChange();
      }
    })
  }

  async addToCart(product) {
    let cartContent = await this._dataService.getCart(this.cookie.get('cart'));
    if (cartContent.length == 0) {
      this._dataService.addCart(
        {
          id: this.cookie.get('cart'),
          products: [product]
        }
      ).then(() => {
        this.cartLength++;
        this.eventChange();
      }, error => {
        console.error(error)
      })
    } else {
      cartContent[0].products.push(product);
      this._dataService.updateCart(
        this.cookie.get('cart'),
        cartContent[0].products
      ).then(() => {
        this.cartLength++;
        this.eventChange();
      }, error => {
        console.error(error)
      })
    }
  }

  async removeFromCart(product) {
    let cartContent = await this._dataService.getCart(this.cookie.get('cart'));
    for (let i = 0; i < cartContent[0].products.length; i++) {
      if (product.id == cartContent[0].products[i].id) {
        cartContent[0].products.splice(i, 1);
        break;
      }
    }
    this._dataService.updateCart(
      this.cookie.get('cart'),
      cartContent[0].products
    ).then(() => {
      this.cartLength--;
      this.eventChange();
    }, error => {
      console.error(error)
    })
  }

  async removeAllFromCart(product) {
    let cartContent = await this._dataService.getCart(this.cookie.get('cart'));
    const length = cartContent[0].products.length;
    for (let i = length-1; i >= 0; i--) {
      if (product.id == cartContent[0].products[i].id) {
        cartContent[0].products.splice(i, 1)
      }
    }
    this.cartLength = cartContent[0].products.length;
    this.eventChange();
    this._dataService.updateCart(
      this.cookie.get('cart'),
      cartContent[0].products
    ).then(() => {
      this.cartLength--;
      this.eventChange();
    }, error => {
      console.error(error)
    })
  }

  getCartContent() {
    return this._dataService.getCart(this.cookie.get('cart'))
  }

  eventChange() {
    this.observableCartLength.next(this.cartLength);
  }

  clear(){
    this.cartLength=0;
    this._dataService.deleteCart(this.cookie.get('cart')).then();
    this.eventChange();
  }


}
