import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderService implements CanActivate {
  private procurer: any;
  private shipmentType: string;
  private payType: string;
  private shipmentPrice:number;
  public orderState=1;
  private pageState=0;

  observablePageState = new BehaviorSubject<number>(this.pageState);
  cast = this.observablePageState.asObservable();
  constructor(private router: Router) { }
  setProcurer(procurer: Object) {
    this.procurer = procurer;
  }
  getProcurer() {
    return this.procurer;
  }
  setShipmentType(shipmentType:string){
    this.pageState=3;
    this.shipmentType=shipmentType;
    switch (this.shipmentType) {
      case 'Normál szállítás a magyar Postával':
        this.shipmentPrice=1300;
        break;
      case 'Elsőbbségi szállítás a magyar Postával':
        this.shipmentPrice=2300;
        break;
    }
    this.eventChange();
  }
  setPayType(payType:string){
    this.payType=payType;
    
  }
  getPayType(){
    return this.payType;
  }
  getShipmentType(){
    return this.shipmentType;
  }
  getShipmentPrice(){
    return this.shipmentPrice;
  }
  getOrderState(){
    return this.orderState;
  }
  setPageState(pageState){
    this.pageState=pageState;
    this.eventChange();
  }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot){
      let page = next.data["page"] as Array<number>;
      console.log(page[0])
      if(page[0]>this.orderState){
        if(this.orderState==1 || this.orderState==0){
          this.router.navigateByUrl('order/address')
        }else if(this.orderState==2){
          this.router.navigateByUrl('order/shipping')
        }
        return false;
      }else{
        return true;
      }
    }

    clear(){
      this.orderState=1;
      this.pageState=0;
      this.procurer=undefined;
      this.shipmentType=undefined;
      this.payType= undefined;
      this.shipmentPrice= undefined;
      this.eventChange();
    }
    
    eventChange() {
      this.observablePageState.next(this.pageState);
    }  
  
}
