import { Injectable } from '@angular/core';
import { MainCategoryApi, SubCategoryApi, ProductApi, CartApi, OrderApi, OpinionApi, PersonApi} from '../shared/sdk'
import { Observable } from 'rxjs';
import { shareReplay } from 'rxjs/operators';

@Injectable()
export class DataService {



  constructor(private personApi:PersonApi, private opinionApi:OpinionApi,private orderApi:OrderApi, private productApi:ProductApi,private mainCategoryApi:MainCategoryApi, private subCategoryApi:SubCategoryApi, private cartApi:CartApi) { }

  getMainCategories(): Observable<any[]>{
    return this.mainCategoryApi.getMainCategory(null,null).pipe( shareReplay(10))
  }

  getSubCategories(): Observable<any[]>{
    return this.subCategoryApi.getSubCategory(null,null,null).pipe( shareReplay(10))
  }

  getSubCategoriesByMainCategoryId(mainCategoryId:string): Observable<any[]>{
    return this.subCategoryApi.getSubCategory(null,null,mainCategoryId).pipe( shareReplay(10));
  }

  addMainCategory(maincategory:Object){
    return this.mainCategoryApi.addMainCategory(maincategory)
  }

  addSubCategory(subcategory:Object){
    return this.subCategoryApi.addSubCategory(subcategory)
  }

  addProduct(product:Object){
    return this.productApi.addProduct(product)
  }

  getProducts(): Observable<any[]>{
    return this.productApi.getProduct(null,null,null,null,null,null,null,null,100000).pipe( shareReplay(10))
  }
  getProductsByCategory(maincategoryId,subcategoryId): Observable<any[]>{
    return this.productApi.getProduct(null,null,maincategoryId ? maincategoryId : null,subcategoryId ? subcategoryId : null,null,null,null,null,100000).pipe( shareReplay(10));
  }
  getProductByPrice(from,to): Observable<any[]>{
    return this.productApi.getProduct(null,null,null,null,from,to,null,null,100000).pipe( shareReplay(10));
  }
  getProductByName(name: string){
    return this.productApi.getProduct(null,name,null,null,null,null,null,null,10)
  }
  getCart(id): Promise<any>{
    return this.cartApi.getCart(id).toPromise();
  }

  addCart(body): Promise<any>{
    return this.cartApi.addCart(body).toPromise()
  }
  updateCart(id,body): Promise<any>{
    return this.cartApi.updateCart(id, body).toPromise()
  }
  deleteCart(id){
    return this.cartApi.deleteCart(id).toPromise();
  }
  order(body):Promise<any>{
    return this.orderApi.addOrder(body).toPromise();
  }
  getOrders(){
    return this.orderApi.getOrder(null,null,null,null,null,null,null);
  }

  search(name){
    return this.productApi.search(name);
  }

  updateOrder(id,status?,arriveDate?){
    return this.orderApi.updateOrder(id,null,status ? status : null,null,null,arriveDate ? arriveDate : null).toPromise();
  }
  getProductById(id){
    return this.productApi.getProduct(id,null,null,null,null,null,null,null,1)
  }
  addRating(body){
    return this.opinionApi.addOpinion(body);
  }
  getRatings(productId){
    return this.opinionApi.getOpinion(null,null,null,productId);
  }

  getUserByEmail(email){
    return this.personApi.getPerson(null,email).toPromise();
  }

  updateUser(body){
    return this.personApi.updatePerson(body.id,body.firstName,body.lastName,body.email,body.phone,body.billingCity,body.billingPostCode,body.billingStreet,body.shippingCity,body.shippingPostCode,body.shippingStreet,body.companyName,body.companyTax, body.profileImage);
  }

  createUser(body){
    return this.personApi.addPerson(body);
  }
  getOrdersByEmail(email){
    return this.orderApi.getOrder(null,null,null,null,null,null,email);
  }
  
  updateProduct(id,name,about,small_about,pictrues,price,dealer,available){
    return this.productApi.updateProduct(id,name,about,small_about,pictrues,price,dealer,available);
  }
  deleteProduct(id){
    return this.productApi.deleteProduct(id);
  }
}