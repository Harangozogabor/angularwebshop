import { Component, OnInit, Input } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { DataService } from '../services/data.service';
import { AuthService } from '../services/auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { OrderService } from '../services/order.service';
import { Person } from '../person';
import { MatSnackBar, MatStepper } from '@angular/material';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { FileUploader, FileItem, FileLikeObject } from 'ng2-file-upload';
import { Observable, BehaviorSubject } from 'rxjs';
declare function require(name: string);
const uuidv1 = require('uuid/v1');
const URLUpload = environment.URLUpload;
const URLDownload = environment.URLDownload;
@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {
  @Input() inshopping = false;
  @Input() stepper: MatStepper;
  @Input() emitter;
  generatedId: string;
  j = 0;
  i = 0;
  public uploader: FileUploader = new FileUploader({
    url: URLUpload,
    isHTML5: true,
    allowedMimeType: ['image/png', 'image/jpg', 'image/jpeg', 'image/gif'],
    maxFileSize: 10 * 1024 * 1024 // 10 MB
  });
  uploadedImage: File;
  imgUrl: String;
  details = 'details';
  welcome = "";
  user: Person = {
    id: '',
    firstName: "",
    lastName: "",
    phone: "",
    email: "",
    billingPostCode: "",
    billingCity: "",
    billingStreet: "",
    shippingPostCode: "",
    shippingCity: "",
    shippingStreet: "",
    companyName: "",
    companyTax: "",
    profileImage: ""
  };
  id;
  haveAccount = false;
  isTheSame = true;
  isCompany = false;
  validation_messages = {
    'firstName': [
      { type: 'required', message: 'Kötelező megadnia a vezetéknevét!' }
    ],
    'lastName': [
      { type: 'required', message: 'Kötelező megadnia a vezetéknevét!' }
    ],
    'email': [
      { type: 'required', message: 'Kötelező megadnia az email címét!' },
      { type: 'validEmail', message: 'Az email cím már foglalt' },
      { type: 'pattern', message: 'Nem megfelelő email cím!' }
    ],
    'postCode': [
      { type: 'required', message: 'Kötelező megadnia az irányítószámot' },
      { type: 'length', message: 'Nem megfelelő irányítószám!' }
    ],
    'city': [
      { type: 'required', message: 'Kötelező megadnia a várost!' }
    ],
    'street': [
      { type: 'required', message: 'Kötelező megadnia az utca házszámot!' }
    ],
    'phone': [
      { type: 'required', message: 'Kötelező megadnia a telefonszámot!' },
      { type: 'pattern', message: 'Nem megfelelő telefonszám!' }
    ],
    'tax': [
      { type: 'required', message: 'Kötelező megadnia az adószámot!' },
      { type: 'pattern', message: 'Nem megfelelő adószám!' }
    ],
    'name': [
      { type: 'required', message: 'Kötelező megadnia a cég nevét!' }
    ]
  }
  fb = new FormBuilder();
  userShippingForm = this.fb.group({
    postCode: new FormControl('', [Validators.minLength(4), Validators.required]),
    street: new FormControl('', Validators.required),
    city: new FormControl('', Validators.required)
  });
  userBillingForm = this.fb.group({
    lastName: new FormControl('', Validators.required),
    firstName: new FormControl('', Validators.required),
    postCode: new FormControl('', [Validators.minLength(4), Validators.required]),
    city: new FormControl('', Validators.required),
    street: new FormControl('', Validators.required),
    phone: new FormControl('', [Validators.pattern("((?:\\+?3|0)6)(?:-|\\()?(\\d{1,2})(?:-|\\))?(\\d{3})-?(\\d{3,4})"), Validators.required]),
    email: new FormControl('', [Validators.email, Validators.required]),
  });
  userCompanyForm = this.fb.group({
    name: new FormControl('', Validators.required),
    taxNumber: new FormControl('', [Validators.required, Validators.pattern("[0-9]{8}-[0-9]{1}-[0-9]{2}")]),
  });
  constructor(public imgmax: Ng2ImgMaxService, public snackbar: MatSnackBar, private dataService: DataService, private authService: AuthService, private route: ActivatedRoute, private router: Router, private orderService: OrderService, public http: HttpClient) {
    this.route.params.subscribe(params => {
      this.id = params['id'];
    });
  }

  ngOnInit() {
    if (this.inshopping) {
      this.details = '';
    }
    this.authService.castChangedUser.subscribe(() => {
      this.initForms();
      this.authService.getUser() && this.authService.getUser().photoURL ? this.imgUrl = this.authService.getUser().photoURL : '';
    })
    this.authService.endOfAuthObserver.subscribe(() => {
      this.initForms();
      this.authService.getUser() && this.authService.getUser().photoURL ? this.imgUrl = this.authService.getUser().photoURL : '';
    })

  }
  initForms() {
    this.dataService.getUserByEmail(this.authService.getUserEmail()).then(data => {
      if (data.length === 1) {
        this.haveAccount = true;
        this.user = data[0];
        if (!this.imgUrl) {
          this.imgUrl = this.user.profileImage ? URLDownload + this.user.profileImage : null;
        }
        this.makeFormsIfHasAccount(data);
      } else {
        this.haveAccount = false;
        this.makeFormsIfHasNotAccount(data);
      }
    });
  }


  makeFormsIfHasAccount(data){
    this.userBillingForm = this.fb.group({
      lastName: new FormControl(data[0].lastName, Validators.required),
      firstName: new FormControl(data[0].firstName, Validators.required),
      postCode: new FormControl(data[0].billingPostCode, [Validators.minLength(4), Validators.required]),
      city: new FormControl(data[0].billingCity, Validators.required),
      street: new FormControl(data[0].billingStreet, Validators.required),
      phone: new FormControl(data[0].phone, [Validators.pattern("((?:\\+?3|0)6)(?:-|\\()?(\\d{1,2})(?:-|\\))?(\\d{3})-?(\\d{3,4})"), Validators.required]),
      email: new FormControl(data[0].email, [Validators.email, Validators.required]),
    });
    if (data[0].companyTax != '') {
      this.userCompanyForm = this.fb.group({
        name: new FormControl(data[0].companyName, Validators.required),
        taxNumber: new FormControl(data[0].companyTax, [Validators.required, Validators.pattern("[0-9]{8}-[0-9]{1}-[0-9]{2}")]),
      });
      this.isCompany = true;
    }
    if (data[0].billingPostCode !== data[0].shippingPostCode && data[0].billingCity !== data[0].shippingCity && data[0].billingStreet !== data[0].shippingStreet) {
      this.userShippingForm = this.fb.group({
        postCode: new FormControl(data[0].shippingPostCode, [Validators.minLength(4), Validators.required]),
        city: new FormControl(data[0].shippingCity, Validators.required),
        street: new FormControl(data[0].shippingStreet, Validators.required),
      })
      this.isTheSame = false;
    }
    this.initFormObservers();
  }

  makeFormsIfHasNotAccount(user){
    this.userBillingForm = this.fb.group({
      lastName: new FormControl(user && user.displayName ? user.displayName.split(" ")[1] : '', Validators.required),
      firstName: new FormControl(user && user.displayName ? user.displayName.split(" ")[0] : '', Validators.required),
      postCode: new FormControl('', [Validators.minLength(4), Validators.required]),
      city: new FormControl('', Validators.required),
      street: new FormControl('', Validators.required),
      phone: new FormControl('', [Validators.pattern("((?:\\+?3|0)6)(?:-|\\()?(\\d{1,2})(?:-|\\))?(\\d{3})-?(\\d{3,4})"), Validators.required]),
      email: new FormControl(user ? user.email : '', [Validators.email, Validators.required]),
    });
    this.initFormObservers();
  }

  initFormObservers(){
    this.userShippingForm.statusChanges.subscribe(
      result =>{
        if(result==='VALID' && (this.userCompanyForm.valid || !this.isCompany) && (this.userBillingForm.valid || this.isTheSame)){
          this.emitter.emit(true)
        }else{
          this.emitter.emit(false)
        }
      }
    );
    this.userBillingForm.statusChanges.subscribe(
      result =>{
        if(result==='VALID' && (this.userCompanyForm.valid || !this.isCompany) && (this.userShippingForm.valid|| this.isTheSame)){
          this.emitter.emit(true)
        }else{
          this.emitter.emit(false)
        }
      }
    );
    this.userCompanyForm.statusChanges.subscribe(
      result =>{
        if(result==='VALID' && (this.userShippingForm.valid) && (this.userBillingForm.valid || this.isTheSame)){
          this.emitter.emit(true)
        }else{
          this.emitter.emit(false)
        }
      }
    );
  }
  send() {
    this.emitter ? this.emitter.emit(true) : '';
    this.user.id = this.authService.getUser() ? this.authService.getUser().uid : uuidv1();
    this.user.firstName = this.userBillingForm.value.firstName;
    this.user.lastName = this.userBillingForm.value.lastName;
    this.user.email = this.userBillingForm.value.email;
    this.user.phone = this.userBillingForm.value.phone;
    this.user.billingCity = this.userBillingForm.value.city;
    this.user.billingPostCode = this.userBillingForm.value.postCode;
    this.user.billingStreet = this.userBillingForm.value.street;
    this.user.shippingCity = this.userBillingForm.value.city;
    this.user.shippingPostCode = this.userBillingForm.value.postCode;
    this.user.shippingStreet = this.userBillingForm.value.street;
    if (!this.isTheSame) {
      this.user.shippingCity = this.userShippingForm.value.city;
      this.user.shippingPostCode = this.userShippingForm.value.postCode;
      this.user.shippingStreet = this.userShippingForm.value.street;
    }
    if (this.isCompany) {
      this.user.companyName = this.userCompanyForm.value.name;
      this.user.companyTax = this.userCompanyForm.value.taxNumber;
    } else {
      this.user.companyName = "";
      this.user.companyTax = "";
    }
    if (this.haveAccount) {
      this.dataService.updateUser(this.user).subscribe(() => {
        if (this.inshopping) {
          this.orderService.setProcurer(this.user);
          console.log(this.stepper)
          this.stepper.next();
        } else {
          this.snackbar.open("Sikeresen frissítetted a profil adataid!", null, {
            duration: 2000,
          });
          this.router.navigateByUrl('/');
        }

      }
      );

    } else {
      if (this.inshopping) {
        this.orderService.setProcurer(this.user);
        this.stepper.next();
      } else {
        this.user.id = this.authService.getUser() ? this.authService.getUser().uid : uuidv1();
        this.dataService.createUser(this.user).subscribe(() => {
          this.snackbar.open("Sikeresen kitöltötted a profil adataid!", null, {
            duration: 2000,
          });
          this.authService.observableUser.next({ firstName: this.user.lastName, lastName: this.user.firstName })
          this.router.navigateByUrl('/');
        })
      }
    }
  }

  onUploadChange(event, filecompress: number = 0.05) {
    const file = event.srcElement.files;
    this.imgmax.compressImage(file[this.j], filecompress).subscribe((compressedFile) => {
      const fileItem = new FileItem(this.uploader, compressedFile, this.uploader.options);
      const extension = fileItem.file.name.split('.');
      this.generatedId = uuidv1() + '.' + extension[1];
      fileItem.file.name = this.generatedId;
      this.user.profileImage = this.generatedId;
      this.uploader.queue.push(fileItem);
      this.uploader.queue[this.i].upload();
      this.uploader.onCompleteItem = async (item: any, response: any, status: any, headers: any) => {
        this.imgUrl = URLDownload + this.generatedId;
        this.i++;
        this.j++;
        if (this.j < file.length) {
          this.onUploadChange(event);
        } else {
          this.j = 0;
        }

      };
    }, error => {
      filecompress += 0.1;
      this.onUploadChange(event, filecompress);
    })
  }


}
