import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {EMPTY} from 'rxjs';
import { AuthService } from './services/auth.service';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class JwtInterceptor implements HttpInterceptor {
    constructor(private authService:AuthService, private http: HttpClient) { }

    intercept( request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        
        request = request.clone({
            withCredentials: true,
          });

        return next.handle(request).pipe(
            catchError((err: HttpErrorResponse) => {

            if (!(err.error instanceof Error) && "Authorization error, bad token"===err.error.error.message) {
    
              console.log(err);
              this.authService.tokenrefresh();
              debugger;
              return this.http.request(request);
            }
    
            // ...optionally return a default fallback value so app can continue (pick one)
            // which could be a default value (which has to be a HttpResponse here)
            // return Observable.of(new HttpResponse({body: [{name: "Default value..."}]}));
            // or simply an empty observable
            return EMPTY;
          }));
    }
}