import { Component, OnInit, Inject } from '@angular/core';
import { CartService } from '../services/cart.service';
import { OrderService } from '../services/order.service';
import { DataService } from '../services/data.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { NgxUiLoaderService } from 'ngx-ui-loader';

@Component({
  selector: 'app-sum',
  templateUrl: './sum.component.html',
  styleUrls: ['./sum.component.css']
})
export class SumComponent implements OnInit {
  shipmentPrice: number;
  shippingType: string;
  basketContent = [

  ];
  sum = 0;
  URLDownload = environment.URLDownload;
  constructor(private ngxService: NgxUiLoaderService,private cartService: CartService, private orderService: OrderService, private dataService: DataService, public dialog: MatDialog) { }


  ngOnInit() { 
    this.cartService.cast.subscribe(()=>{
      this.initializer();
    })
  }

  initializer(){
    for (let i = 0; i < this.basketContent.length; i++) {
      this.basketContent[i].amount=0;
    }
    this.cartService.getCartContent().then(data => {
      this.sum=0;
      if (data.length != 0) {
        for (let i = 0; i < data[0].products.length; i++) {
          this.sum += data[0].products[i].price;
          this.basketIsConatin(data[0].products[i]);
        }
      }
    })
    this.orderService.cast.subscribe(orderState => {
      if (orderState == 3) {
        this.shippingType = this.orderService.getShipmentType();
        this.shipmentPrice = this.orderService.getShipmentPrice();
      }
    })
  }

  basketIsConatin(product) {
    for (let i = 0; i < this.basketContent.length; i++) {
      if (product.id == this.basketContent[i].product.id) {
        this.basketContent[i].amount++;
        return;
      }
    }
    this.basketContent.push({ product: product, amount: 1 })
  }

  order() {
    this.ngxService.start();
    let products = [];
    for (let i = 0; i < this.basketContent.length; i++) {
      let product = { id: this.basketContent[i].product.id, amount: this.basketContent[i].amount };
      if(products.length==0){
        products.push(product)
      }
      for(let j =0; j<products.length;j++){
        if(products[j].id!=product.id){
          products.push(product)
        }
      }
    }
    let paymentType = this.orderService.getPayType();
    let shippingType = this.orderService.getShipmentType();
    let buyer = this.orderService.getProcurer();
    this.dataService.order({ products: products, paymentType: paymentType, shippingType: shippingType, buyer: buyer }).then(data => {
      this.ngxService.stop();
      this.openDialog('success');
      this.orderService.clear();
      this.cartService.clear();
    },
      error => {
        this.ngxService.stop();
        this.openDialog('error');
      })

  }

  openDialog(type: string): void {
    const dialogRef = this.dialog.open(ConfirmDialog, {
      width: '250px',
      data: { type: type }
    });

    dialogRef.afterClosed().subscribe(result => {

    });
  }
}
@Component({
  selector: 'confirm-dialog',
  templateUrl: 'confirmDialog.html',
})
export class ConfirmDialog {

  constructor(
    public dialogRef: MatDialogRef<ConfirmDialog>, @Inject(MAT_DIALOG_DATA) public data, private router: Router) { }

  onNoClick(): void {
    this.dialogRef.close();
    this.router.navigateByUrl('/');
  }
}
