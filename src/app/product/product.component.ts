import { Component, OnInit, ViewChild } from '@angular/core';
import { NgxGalleryOptions, NgxGalleryImage, NgxGalleryAnimation } from 'ngx-gallery';
import { GALLERY_CONF, GALLERY_IMAGE, NgxImageGalleryComponent } from "ngx-image-gallery";
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data.service';
import { CartService } from '../services/cart.service';
import { environment } from 'src/environments/environment';
import { MatSnackBar } from '@angular/material';
import {DomSanitizer} from '@angular/platform-browser';
import { AuthService } from '../services/auth.service';
import { switchMap } from 'rxjs/operators';


const URLDownload = environment.URLDownload;
export const DEMO_GALLERY_CONF_INLINE: GALLERY_CONF = {
  imageOffset: '0px',
  imagePointer: true,
  showDeleteControl: false,
  showCloseControl: false,
  showExtUrlControl: false,
  closeOnEsc: false,
  showImageTitle: false,
  inline: true,
  backdropColor: 'default'
};
export const DEMO_GALLERY_CONF_MODAL: GALLERY_CONF = {
  imageOffset: '0px',
  imagePointer: true,
  showDeleteControl: false,
  showCloseControl: true,
  showExtUrlControl: false,
  closeOnEsc: true,
  showImageTitle: false,
  inline: false,
  backdropColor: 'rgba(0,0,0,0.5)',
  thumbnailSize: 100,
  reactToKeyboard: true,
  reactToMouseWheel: false
};

export const DEMO_GALLERY_CONF: GALLERY_CONF = {
  imageOffset: '0px',
  showDeleteControl: false,
  showCloseControl: true,
  showImageTitle: false,
  inline: false,
  backdropColor: 'rgba(13,13,14,0.85)'
};

// gallery images
export const DEMO_GALLERY_IMAGE: GALLERY_IMAGE[] = [
  { 
      url:  '../../assets/images/de.png',
      altText: '',
      title: '',
      thumbnailUrl:'../../assets/images/de.png'
  }
];

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.scss']
})
export class ProductComponent implements OnInit {
  constructor(private authService:AuthService, private sanitizer: DomSanitizer, private route: ActivatedRoute, private dataService: DataService, private cartService: CartService, private snackBar:MatSnackBar) { }

  public showConf: boolean = true;
  product;
  ratings;
  ratingStat={
    sum:0,
    avg:'',
    stars:0
  }
  rating = {
    id: '',
    product: '',
    user:'',
    about: '',
    rate: 5,
    date: new Date().toISOString()
  }
  id;
  imgUrl:String;
  @ViewChild('ngxImageGallery') ngxImageGallery: NgxImageGalleryComponent;
  @ViewChild('ngxImageGallery2') ngxImageGallery2: NgxImageGalleryComponent;

  ngOnInit() {
    this.authService.castChangedUser.subscribe(()=>{
      this.authService.getUser() && this.authService.getUser().photoURL ? this.imgUrl=this.authService.getUser().photoURL : '';
    })
    this.authService.endOfAuthObserver.subscribe(()=>{
      this.authService.getUser() && this.authService.getUser().photoURL ? this.imgUrl=this.authService.getUser().photoURL : '';
    })
    this.dataService.getUserByEmail(this.authService.getUserEmail()).then(data => { 
      if ( data.length === 1) {
        this.rating.user = data[0];
        if(!this.imgUrl){
          this.imgUrl = data[0].profileImage ? URLDownload + data[0].profileImage : null;
        }
      }
    });
    this.route.params.pipe(switchMap(params=>{
      this.id=params['id'];
      this.loadRatings(params['id']);
      return  this.dataService.getProductById(params['id']);
    })).subscribe(data => {
      this.product = data[0];
      this.product.about=this.sanitizer.bypassSecurityTrustHtml(this.product.about);
      this.rating.product=this.product;
      this.images.length=0;
      for (let j = 0; j < this.product.pictures.length; j++) {
        let picture = {
          url: URLDownload + this.product.pictures[j],
          altText: '',
          title: '',
          thumbnailUrl: URLDownload + this.product.pictures[j]
        }
        this.images.push(picture)
      }
      if(this.product.pictures.length==0){
        let picture = {
          url:  '../../assets/images/de.png',
          altText: '',
          title: '',
          thumbnailUrl:'../../assets/images/de.png'
        }
        this.images.push(picture)
      }
    })

  }


  conf: GALLERY_CONF = DEMO_GALLERY_CONF_INLINE;
  conf2: GALLERY_CONF = DEMO_GALLERY_CONF_MODAL;
  // gallery images
  images: GALLERY_IMAGE[] = DEMO_GALLERY_IMAGE;

  loadRatings(id){
    this.ratingStat={
      sum:0,
      avg:'',
      stars:0
    }
    this.ratings=this.dataService.getRatings(id);
    this.dataService.getRatings(id).subscribe(data=>{
      let helperSum=0;
      data.forEach(element => {
        this.ratingStat.sum++;
        helperSum+=element.rate;
      });
      if(this.ratingStat.sum==0){
        this.ratingStat.avg='0';
      }else{
        this.ratingStat.avg=(helperSum/this.ratingStat.sum).toFixed(2); 
      }
      this.ratingStat.stars=Math.round(helperSum/this.ratingStat.sum);
    })
  }
  // METHODS
  // open gallery
  openGallery(index: number = 0) {
    this.ngxImageGallery.open(index);
  }

  // close gallery
  closeGallery() {
    // this.ngxImageGallery.close();
  }

  // set new active(visible) image in gallery
  newImage(index: number = 0) {
    // this.ngxImageGallery.setActiveImage(index);
  }

  // next image in gallery
  nextImage() {
    this.ngxImageGallery.next();
  }

  // prev image in gallery
  prevImage() {
    this.ngxImageGallery.prev();
  }

  /**************************************************/


  // callback on gallery image clicked
  galleryImageClicked(index) {
    this.ngxImageGallery2.open(index);
  }

  addToCart() {
    this.cartService.addToCart(this.product);
  }
  onRatingChanged(e) {
    this.rating.rate=e;
  }

  sendRating(){
    this.dataService.addRating(this.rating).subscribe(()=>{
      this.rating.about='';
      this.snackBar.open("Köszönjük a vélemény nyilvánítást!",null, {
        duration: 2000,
      });
      this.loadRatings(this.id);
    });
  }


}
