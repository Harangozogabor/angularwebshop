import { Component, OnInit, ViewChild, Inject } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource, MatSnackBar } from '@angular/material';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { Validators, FormBuilder, FormControl } from '@angular/forms';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { FileUploader, FileItem, FileLikeObject } from 'ng2-file-upload';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { Product } from '../shared/sdk';
import { BehaviorSubject } from 'rxjs';
import { switchMap } from 'rxjs/operators';
declare function require(name: string);
const uuidv1 = require('uuid/v1');
@Component({
  selector: 'app-update-product',
  templateUrl: './update-product.component.html',
  styleUrls: ['./update-product.component.css']
})
export class UpdateProductComponent implements OnInit {
  displayedColumns: string[] = ['edit', 'delete', 'id', 'name', 'category', 'subCategory'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  adminStuffs;
  constructor(private authService: AuthService, private _dataService: DataService, private _router: Router, public dialog: MatDialog) {
    this.init();
  }

  ngOnInit() {
  }

  init(){
      this._dataService.getProducts().subscribe(data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
        this.dataSource.sortingDataAccessor = (item, property) => {
          switch (property) {
            case 'category': return item.subCategory.mainCategory.name;
            case 'subCategory': return item.subCategory.name;
            default: return item[property];
          }
        };
      })
  }
  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  delete(product) {
    this._dataService.deleteProduct(product.id).pipe(switchMap(()=>{
      return  this._dataService.getProducts();
    })).subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

    });
  }


  openDialog(product): void {
    const dialogRef = this.dialog.open(ProductEditorDialog, {
      width: '80%',
      data: { product: product}
    });

    dialogRef.afterClosed().subscribe(result => {
      this.init();
    });
  }

}

@Component({
  selector: 'editorDialog',
  templateUrl: 'productEditorModal.html',
})
export class ProductEditorDialog implements OnInit{
  quill;
  product: Product = new Product();
  fb = new FormBuilder();
  productForm = this.fb.group({
    id: new FormControl(),
    subCategory: new FormControl(null,Validators.required),
    name: new FormControl('', Validators.required),
    small_about: new FormControl(),
    price: new FormControl(0, Validators.required),
    dealer: new FormControl('', Validators.required),
    available: new FormControl(0, Validators.required),
  });
  generatedId: string;
  j = 0;
  i = 0;
  showImage = environment.URLDownload + '3f8c98f0-dbc1-11e8-b60e-3db7f54787f7.png';
  public uploader: FileUploader = new FileUploader({
    url: environment.URLUpload,
    isHTML5: true,
    allowedMimeType: ['image/png', 'image/jpg', 'image/jpeg', 'image/gif'],
    maxFileSize: 10 * 1024 * 1024 // 10 MB
  });
  downloadURL = environment.URLDownload;
  deleteURL = environment.URLDelete;
  filenames = [];
  constructor(
    private snackbar:MatSnackBar, public dialogRef: MatDialogRef<ProductEditorDialog>, @Inject(MAT_DIALOG_DATA) public data, private router: Router, private dataService: DataService, private http: HttpClient, public imgmax: Ng2ImgMaxService) {
  }
  ngOnInit(): void {
    this.productForm.setValue({
      id:this.data.product.id,
      subCategory:  this.data.product.subCategory,
      name: this.data.product.name,
      small_about: this.data.product.small_about,
      price: this.data.product.price,
      dealer: this.data.product.dealer,
      available: this.data.product.available,
    })
  }
  cancel(): void {
    this.dialogRef.close();
  }
  deletePicture(picture: string) {
    let index = this.data.product.pictures.indexOf(picture);
    if (index > -1) {
      this.data.product.pictures.splice(index, 1);
    }
    this.http.delete(this.deleteURL + picture).subscribe();
  }
  onUploadChange(event) {
    const file = event.srcElement.files;
    this.imgmax.compressImage(file[this.j], 0.05).subscribe((compressedFile) => {
      const fileItem = new FileItem(this.uploader, compressedFile, this.uploader.options);
      const extension = fileItem.file.name.split('.');
      this.filenames.push(fileItem.file.name);
      this.generatedId = uuidv1() + '.' + extension[1];
      fileItem.file.name = this.generatedId;
      this.uploader.queue.push(fileItem);
      this.uploader.queue[this.i].upload();
      this.uploader.onCompleteItem = async (item: any, response: any, status: any, headers: any) => {
        this.showImage = environment.URLDownload + this.generatedId;
        this.i++;
        this.j++;
        if (this.j < file.length) {
          this.onUploadChange(event);
        } else {
          this.j = 0;
        }
        this.data.product.pictures.push(this.generatedId);
      };
    });
  }
  getQuill(quill){
    this.quill=quill;
    this.quill.root.innerHTML=this.data.product.about;
  }
  onSubmitProduct() {
    console.log(this.product)
    this.dataService.updateProduct(this.data.product.id,this.productForm.value.name, this.quill.container.firstChild.innerHTML, this.productForm.value.small_about, this.data.product.pictures, this.productForm.value.price, this.productForm.value.dealer, this.productForm.value.available).subscribe(
      ()=>{
        this.productForm.reset();
        this.product = new Product();
        this.product.pictures = [];
        this.snackbar.open("Sikeres feltöltés!",null, {
          duration: 2000,
        });
      },error=>{
        this.snackbar.open("Sikertelen feltöltés!",null, {
          duration: 2000,
        });
        console.error(error);
      }
    );
    this.dialogRef.close();
  }

}
