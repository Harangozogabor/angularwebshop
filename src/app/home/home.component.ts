import { Component, OnInit } from '@angular/core';
import { animate, group, query, style, transition, trigger } from '@angular/animations';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  animations: [
    trigger('slideAnimation', [
      transition(':increment', group([
        query(':enter', [
          style({
            transform: 'translateX(100%)'
          }),
          animate('0.5s ease-out', style('*'))
        ]),
        query(':leave', [
          animate('0.5s ease-out', style({
            transform: 'translateX(-100%)'
          }))
        ])
      ])),
      transition(':decrement', group([
        query(':enter', [
          style({
            transform: 'translateX(-100%)'
          }),
          animate('0.5s ease-out', style('*'))
        ]),
        query(':leave', [
          animate('0.5s ease-out', style({
            transform: 'translateX(100%)'
          }))
        ])
      ]))
    ])
  ]
})
export class HomeComponent implements OnInit {
  categories = [
    {
      title: 'Rajtad a sor',
      subTitle: 'Ünnepeld az Air Max Day-t főiskolai stílusú kényelemben.',
      pictures: ['../../assets/images/6.jpg', '../../assets/images/1.jpg', '../../assets/images/4.jpg'],
      imageIndex: 0
    },
    {
      title: 'BELE A KÉKBE',
      subTitle: 'Készülj fel a Nike Air Max 270 „Blue Fury" cipőre.',
      pictures: ['../../assets/images/3.jpg', '../../assets/images/5.jpg', '../../assets/images/2.jpg'],
      imageIndex: 0
    },
    {
      title: 'Rajtad a sor',
      subTitle: 'Ünnepeld az Air Max Day-t főiskolai stílusú kényelemben.',
      pictures: ['../../assets/images/1.jpg', 'https://picsum.photos/500/350/?random', 'https://picsum.photos/500/350/?random'],
      imageIndex: 0
    },
    {
      title: 'BELE A KÉKBE',
      subTitle: 'Készülj fel a Nike Air Max 270 „Blue Fury" cipőre.',
      pictures: ['https://picsum.photos/500/350/?random', 'https://picsum.photos/500/350/?random', 'https://picsum.photos/500/350/?random'],
      imageIndex: 0
    },
    {
      title: 'Rajtad a sor',
      subTitle: 'Ünnepeld az Air Max Day-t főiskolai stílusú kényelemben.',
      pictures: ['https://picsum.photos/500/350/?random', 'https://picsum.photos/500/350/?random', 'https://picsum.photos/500/350/?random'],
      imageIndex: 0
    },
    {
      title: 'BELE A KÉKBE',
      subTitle: 'Készülj fel a Nike Air Max 270 „Blue Fury" cipőre.',
      pictures: ['https://picsum.photos/500/350/?random', 'https://picsum.photos/500/350/?random', 'https://picsum.photos/500/350/?random'],
      imageIndex: 0
    },
    {
      title: 'Rajtad a sor',
      subTitle: 'Ünnepeld az Air Max Day-t főiskolai stílusú kényelemben.',
      pictures: ['https://picsum.photos/500/350/?random', 'https://picsum.photos/500/350/?random', 'https://picsum.photos/500/350/?random'],
      imageIndex: 0
    },
    {
      title: 'BELE A KÉKBE',
      subTitle: 'Készülj fel a Nike Air Max 270 „Blue Fury" cipőre.',
      pictures: ['https://picsum.photos/500/350/?random', 'https://picsum.photos/500/350/?random', 'https://picsum.photos/500/350/?random'],
      imageIndex: 0
    }
  ];
  constructor() { }

  ngOnInit() {
    this.preloadImages();
    this.later();
  }

  setCurrentSlideIndex(object, index) {
    object.imageIndex = index;
  }
  isCurrentSlideIndex(object, index) {
    return object.imageIndex === index;
  }
  next(object) {
    if (object.pictures.length > object.imageIndex + 1) {
      object.imageIndex++;
    } else {
      object.imageIndex = 0;
    }
  }
  later() {
    setTimeout(() => {
      this.categories.forEach(category => {
        this.next(category);
      })
      this.later();
    }, 3000);
  }
  preloadImages() {
    this.categories.forEach(category => {
      category.pictures.forEach(picture => {
        (new Image()).src = picture;
      })
    })
  }
}
