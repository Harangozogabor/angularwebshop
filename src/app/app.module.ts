import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ErrorHandler, Injector } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from '@angular/common/http';
import { AppComponent } from './app.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent, PasswordResetDialog } from './login/login.component';
import { AdminComponent, EditorDialog } from './admin/admin.component';
import { StuffsComponent } from './stuffs/stuffs.component';
import { AppRoutingModule } from './app-routing.module';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './auth.guard';
import { DataService } from './services/data.service';
import { SDKBrowserModule } from './shared/sdk';
import { BasketComponent } from './basket/basket.component';
import { NavigationComponent } from './navigation/navigation.component';
import { DesktopMenuComponent } from './desktop-menu/desktop-menu.component';
import { UserMenuComponent } from './user-menu/user-menu.component';
import { StarRatingComponent } from './star-rating/star-rating.component';
import { ProductComponent } from './product/product.component';
import { NgxGalleryModule } from 'ngx-gallery';
import { UploadComponent } from './upload/upload.component';
import { Ng2ImgMaxModule } from 'ng2-img-max';
import { OrderComponent } from './order/order.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {FlexLayoutModule} from '@angular/flex-layout';
import { AppMaterialModule } from './app.material.module';
import { AddressComponent } from './address/address.component';
import { ShippingComponent } from './shipping/shipping.component';
import { SumComponent, ConfirmDialog } from './sum/sum.component';
import { OrderNavigationComponent } from './order-navigation/order-navigation.component';
import { CardComponent } from './card/card.component';
import { CookieService } from 'ngx-cookie-service';
import { CartService } from './services/cart.service';
import { Ng5SliderModule } from 'ng5-slider';
import { NgxImageGalleryModule } from 'ngx-image-gallery';
import { environment } from '../environments/environment';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { UserDetailsComponent } from './user-details/user-details.component';
import { MyOrdersComponent,ConfirmDialog2 } from './my-orders/my-orders.component';
import { UpdateProductComponent,ProductEditorDialog } from './update-product/update-product.component';
import { PhoneLoginComponent } from './phone-login/phone-login.component';
import { HttpModule, Http } from '@angular/http';
import { QuillModule } from 'ngx-quill';
import { QuillInitializeService } from "./services/quillInitialize.service";
import { NgxUiLoaderModule } from  'ngx-ui-loader';
import { RichTextEditorComponent } from './rich-text-editor/rich-text-editor.component';
import {TranslateModule, TranslateLoader} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import { MyErrorHandler } from './errorhandler';
import { HomeComponent } from './home/home.component';
import { JwtInterceptor } from './jwIInterceptor';
import { CachingInterceptor } from './cacheInterceptor';
import { CacheService } from './cache.service';
import { ServiceWorkerModule } from '@angular/service-worker';

export function HttpLoaderFactory(httpClient: HttpClient) {
  return new TranslateHttpLoader(httpClient);
}

@NgModule({
  declarations: [
    UserDetailsComponent,
    AppComponent,
    RegisterComponent,
    LoginComponent,
    AdminComponent,
    StuffsComponent,
    UserMenuComponent,
    NavigationComponent,
    DesktopMenuComponent,
    BasketComponent,
    StarRatingComponent,
    ProductComponent,
    UploadComponent,
    OrderComponent,
    AddressComponent,
    ShippingComponent,
    SumComponent,
    OrderNavigationComponent,
    CardComponent,
    ConfirmDialog,
    PasswordResetDialog,
    EditorDialog,
    MyOrdersComponent,
    ConfirmDialog2,
    UpdateProductComponent,
    ProductEditorDialog,
    PhoneLoginComponent,
    RichTextEditorComponent,
    HomeComponent
  ],
  imports: [
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    QuillModule,
    NgxUiLoaderModule,
    HttpModule,
    NgxImageGalleryModule,
    Ng5SliderModule,
    AppMaterialModule,
    Ng2ImgMaxModule,
    NgxGalleryModule,
    BrowserModule,
    FlexLayoutModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    QuillModule,
    SDKBrowserModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production })
  ],
  providers: [AuthService, AuthGuard, DataService,CookieService,CartService,QuillInitializeService,CacheService,
    {provide: ErrorHandler, useClass: MyErrorHandler,deps: [Injector]},
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CachingInterceptor,
      multi: true,
    }],
  bootstrap: [AppComponent],
  entryComponents: [ConfirmDialog, EditorDialog,ConfirmDialog2,ProductEditorDialog,PasswordResetDialog]
})
export class AppModule { }
