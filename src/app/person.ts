export interface Person {
    id:String,
    firstName: String,
    lastName: String,
    phone: String,
    email: String,
    billingPostCode: String,
    billingCity: String,
    billingStreet: String,
    shippingPostCode: String,
    shippingCity: String,
    shippingStreet: String,
    companyName: String,
    companyTax: String,
    profileImage: String
}
