import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  validation_messages = {
    'password': [
      { type: 'required', message: 'Kötelező megadnia a jelszót!' },
      { type: 'MatchPassword', message: 'Nem megegyező jelszavak!' }
    ],
    'confirmpassword': [
      { type: 'required', message: 'Kötelező megadnia a jelszót!' },
      { type: 'MatchPassword', message: 'Nem megegyező jelszavak!' }
    ],
    'email': [
      { type: 'required', message: 'Kötelező megadnia az email címét!' },
      { type: 'validEmail', message: 'Az email cím már foglalt' },
      { type: 'pattern', message: 'Nem megfelelő email cím!' }
    ]
  }
  constructor(private ngxService: NgxUiLoaderService, private authService: AuthService, private dataService: DataService, private router: Router, public snackbar: MatSnackBar) { }
  fb = new FormBuilder();
  userForm = this.fb.group({
    password: new FormControl('', Validators.required),
    confirmpassword: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.email, Validators.required]),
  },
    {
      validator: PasswordValidation.MatchPassword
    });
  ngOnInit() {
  }

  send() {
    this.ngxService.start();
    this.dataService.getUserByEmail(this.userForm.value.email).then(data => {
      this.ngxService.stop();
      if (data.length != 0) {
        this.snackbar.open("Már van ilyen felhasználó!", null, {
          duration: 2000,
        });
      } else {
        this.ngxService.stop();
        return this.authService.register(this.userForm.value.email, this.userForm.value.password)
      }
    }).then(() => {
      this.authService.sendEmailVerification();
      this.snackbar.open("A megadott email címre elküldtük a megerősítő emailt!", null, {
        duration: 2000,
      });
      this.router.navigateByUrl("/login");

    })

  }
}
// regisztrál, kap egy linket azzal megerősíti a regisztrációt, utána bejelentkezik és átdobjuk az adatlap kitöltésre
import { AbstractControl } from '@angular/forms';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material';
import { NgxUiLoaderService } from 'ngx-ui-loader';
export class PasswordValidation {

  static MatchPassword(AC: AbstractControl) {
    let password = AC.get('password').value; // to get value in input tag
    let confirmPassword = AC.get('confirmpassword').value; // to get value in input tag
    if (password != confirmPassword) {
      AC.get('confirmpassword').setErrors({ MatchPassword: true })
    } else {
      return null
    }
  }
}