/* tslint:disable */
import { Injectable } from '@angular/core';
import { User } from '../../models/User';
import { Product } from '../../models/Product';
import { MainCategory } from '../../models/MainCategory';
import { SubCategory } from '../../models/SubCategory';
import { Order } from '../../models/Order';
import { Opinion } from '../../models/Opinion';
import { Person } from '../../models/Person';
import { Cart } from '../../models/Cart';
import { Container } from '../../models/Container';

export interface Models { [name: string]: any }

@Injectable()
export class SDKModels {

  private models: Models = {
    User: User,
    Product: Product,
    MainCategory: MainCategory,
    SubCategory: SubCategory,
    Order: Order,
    Opinion: Opinion,
    Person: Person,
    Cart: Cart,
    Container: Container,
    
  };

  public get(modelName: string): any {
    return this.models[modelName];
  }

  public getAll(): Models {
    return this.models;
  }

  public getModelNames(): string[] {
    return Object.keys(this.models);
  }
}
