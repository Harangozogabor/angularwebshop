/* tslint:disable */
import { Injectable, Inject, Optional } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SDKModels } from './SDKModels';
import { BaseLoopBackApi } from '../core/base.service';
import { LoopBackConfig } from '../../lb.config';
import { LoopBackAuth } from '../core/auth.service';
import { LoopBackFilter,  } from '../../models/BaseModels';
import { ErrorHandler } from '../core/error.service';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Person } from '../../models/Person';
import { SocketConnection } from '../../sockets/socket.connections';


/**
 * Api services for the `Person` model.
 */
@Injectable()
export class PersonApi extends BaseLoopBackApi {

  constructor(
    @Inject(HttpClient) protected http: HttpClient,
    @Inject(SocketConnection) protected connection: SocketConnection,
    @Inject(SDKModels) protected models: SDKModels,
    @Inject(LoopBackAuth) protected auth: LoopBackAuth,
    @Optional() @Inject(ErrorHandler) protected errorHandler: ErrorHandler
  ) {
    super(http,  connection,  models, auth, errorHandler);
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {object} data Request data.
   *
   * This method expects a subset of model properties as request parameters.
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * This method returns no data.
   */
  public addPerson(body: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/People/addPerson";
    let _routeParams: any = {};
    let _postBody: any = {
      body: body
    };
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {string} id 
   *
   * @param {string} email 
   *
   * @returns {object[]} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * OK
   */
  public getPerson(id: any = {}, email: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/People/getPerson";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof id !== 'undefined' && id !== null) _urlParams.id = id;
    if (typeof email !== 'undefined' && email !== null) _urlParams.email = email;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {string} id 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * This method returns no data.
   */
  public deletePerson(id: any, customHeaders?: Function): Observable<any> {
    let _method: string = "DELETE";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/People/deletePerson";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof id !== 'undefined' && id !== null) _urlParams.id = id;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {string} id 
   *
   * @param {string} firstName 
   *
   * @param {string} lastName 
   *
   * @param {string} email 
   *
   * @param {string} phone 
   *
   * @param {string} billingCity 
   *
   * @param {string} billingPostCode 
   *
   * @param {string} billingStreet 
   *
   * @param {string} shippingCity 
   *
   * @param {string} shippingPostCode 
   *
   * @param {string} shippingStreet 
   *
   * @param {string} companyName 
   *
   * @param {string} companyTax 
   *
   * @param {object} data Request data.
   *
   * This method does not accept any data. Supply an empty object.
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * This method returns no data.
   */
  public updatePerson(id: any, firstName: any, lastName: any, email: any, phone: any, billingCity: any, billingPostCode: any, billingStreet: any, shippingCity: any, shippingPostCode: any, shippingStreet: any, companyName: any, companyTax: any,profileImage: any, customHeaders?: Function): Observable<any> {
    let _method: string = "PUT";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/People/:id";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof id !== 'undefined' && id !== null) _urlParams.id = id;
    if (typeof firstName !== 'undefined' && firstName !== null) _urlParams.firstName = firstName;
    if (typeof lastName !== 'undefined' && lastName !== null) _urlParams.lastName = lastName;
    if (typeof email !== 'undefined' && email !== null) _urlParams.email = email;
    if (typeof phone !== 'undefined' && phone !== null) _urlParams.phone = phone;
    if (typeof billingCity !== 'undefined' && billingCity !== null) _urlParams.billingCity = billingCity;
    if (typeof billingPostCode !== 'undefined' && billingPostCode !== null) _urlParams.billingPostCode = billingPostCode;
    if (typeof billingStreet !== 'undefined' && billingStreet !== null) _urlParams.billingStreet = billingStreet;
    if (typeof shippingCity !== 'undefined' && shippingCity !== null) _urlParams.shippingCity = shippingCity;
    if (typeof shippingPostCode !== 'undefined' && shippingPostCode !== null) _urlParams.shippingPostCode = shippingPostCode;
    if (typeof shippingStreet !== 'undefined' && shippingStreet !== null) _urlParams.shippingStreet = shippingStreet;
    if (typeof companyName !== 'undefined' && companyName !== null) _urlParams.companyName = companyName;
    if (typeof companyTax !== 'undefined' && companyTax !== null) _urlParams.companyTax = companyTax;
    if (typeof profileImage !== 'undefined' && profileImage !== null) _urlParams.profileImage = profileImage;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * The name of the model represented by this $resource,
   * i.e. `Person`.
   */
  public getModelName() {
    return "Person";
  }
}
