/* tslint:disable */
import { Injectable, Inject, Optional } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { SDKModels } from './SDKModels';
import { BaseLoopBackApi } from '../core/base.service';
import { LoopBackConfig } from '../../lb.config';
import { LoopBackAuth } from '../core/auth.service';
import { LoopBackFilter,  } from '../../models/BaseModels';
import { ErrorHandler } from '../core/error.service';
import { Observable, Subject } from 'rxjs';
import { map } from 'rxjs/operators';
import { Product } from '../../models/Product';
import { SocketConnection } from '../../sockets/socket.connections';


/**
 * Api services for the `Product` model.
 */
@Injectable()
export class ProductApi extends BaseLoopBackApi {

  constructor(
    @Inject(HttpClient) protected http: HttpClient,
    @Inject(SocketConnection) protected connection: SocketConnection,
    @Inject(SDKModels) protected models: SDKModels,
    @Inject(LoopBackAuth) protected auth: LoopBackAuth,
    @Optional() @Inject(ErrorHandler) protected errorHandler: ErrorHandler
  ) {
    super(http,  connection,  models, auth, errorHandler);
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {object} data Request data.
   *
   * This method expects a subset of model properties as request parameters.
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * This method returns no data.
   */
  public addProduct(body: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "POST";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Products/addProduct";
    let _routeParams: any = {};
    let _postBody: any = {
      body: body
    };
    let _urlParams: any = {};
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {string} id 
   *
   * @param {string} name 
   *
   * @param {string} category_id 
   *
   * @param {string} from_price 
   *
   * @param {string} to_price 
   *
   * @param {number} available 
   *
   * @param {string} dealer 
   *
   * @param {number} limit 
   *
   * @returns {object[]} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * OK
   */
  public getProduct(id: any = {}, name: any = {}, mainCategory_id: any = {}, subCategory_id: any = {},from_price: any = {}, to_price: any = {}, available: any = {}, dealer: any = {}, limit: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Products/getProduct";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof id !== 'undefined' && id !== null) _urlParams.id = id;
    if (typeof name !== 'undefined' && name !== null) _urlParams.name = name;
    if (typeof mainCategory_id !== 'undefined' && mainCategory_id !== null) _urlParams.mainCategory_id = mainCategory_id;
    if (typeof subCategory_id !== 'undefined' && subCategory_id !== null) _urlParams.subCategory_id = subCategory_id;
    if (typeof from_price !== 'undefined' && from_price !== null) _urlParams.from_price = from_price;
    if (typeof to_price !== 'undefined' && to_price !== null) _urlParams.to_price = to_price;
    if (typeof available !== 'undefined' && available !== null) _urlParams.available = available;
    if (typeof dealer !== 'undefined' && dealer !== null) _urlParams.dealer = dealer;
    if (typeof limit !== 'undefined' && limit !== null) _urlParams.limit = limit;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }
  
  public search(name: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "GET";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Products/search";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof name !== 'undefined' && name !== null) _urlParams.name = name;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }
  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {string} id 
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * This method returns no data.
   */
  public deleteProduct(id: any, customHeaders?: Function): Observable<any> {
    let _method: string = "DELETE";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Products/deleteProduct";
    let _routeParams: any = {};
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof id !== 'undefined' && id !== null) _urlParams.id = id;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * <em>
         * (The remote method definition does not provide any description.)
         * </em>
   *
   * @param {string} id 
   *
   * @param {string} about 
   *
   * @param {string} pictures 
   *
   * @param {number} price 
   *
   * @param {string} dealer 
   *
   * @param {number} available 
   *
   * @param {object} data Request data.
   *
   * This method does not accept any data. Supply an empty object.
   *
   * @returns {object} An empty reference that will be
   *   populated with the actual data once the response is returned
   *   from the server.
   *
   * This method returns no data.
   */
  public updateProduct(id: any,name: any = {}, about: any = {}, small_about: any = {}, pictures: any = {}, price: any = {}, dealer: any = {}, available: any = {}, customHeaders?: Function): Observable<any> {
    let _method: string = "PUT";
    let _url: string = LoopBackConfig.getPath() + "/" + LoopBackConfig.getApiVersion() +
    "/Products/:id";
    let _routeParams: any = {
      id: id
    };
    let _postBody: any = {};
    let _urlParams: any = {};
    if (typeof id !== 'undefined' && id !== null) _urlParams.id = id;
    if (typeof name !== 'undefined' && name !== null) _urlParams.name = name;
    if (typeof about !== 'undefined' && about !== null) _urlParams.about = about;
    if (typeof small_about !== 'undefined' && small_about !== null) _urlParams.small_about = small_about;
    if (typeof pictures !== 'undefined' && pictures !== null) _urlParams.pictures = pictures;
    if (typeof price !== 'undefined' && price !== null) _urlParams.price = price;
    if (typeof dealer !== 'undefined' && dealer !== null) _urlParams.dealer = dealer;
    if (typeof available !== 'undefined' && available !== null) _urlParams.available = available;
    let result = this.request(_method, _url, _routeParams, _urlParams, _postBody, null, customHeaders);
    return result;
  }

  /**
   * The name of the model represented by this $resource,
   * i.e. `Product`.
   */
  public getModelName() {
    return "Product";
  }
}
