/* tslint:disable */

declare var Object: any;
export interface ProductInterface {
  "id"?: any;
  "subCategory"?: any;
  "name"?: string;
  "about"?: string;
  "small_about"?: string;
  "pictures"?: Array<any>;
  "price"?: number;
  "dealer"?: string;
  "available"?: number;
}

export class Product implements ProductInterface {
  "id": any;
  "subCategory": any;
  "name": string;
  "about": string;
  "small_about": string;
  "pictures": Array<any>;
  "price": number;
  "dealer": string;
  "available": number;
  constructor(data?: ProductInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Product`.
   */
  public static getModelName() {
    return "Product";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Product for dynamic purposes.
  **/
  public static factory(data: ProductInterface): Product{
    return new Product(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Product',
      plural: 'Products',
      path: 'Products',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "subCategory": {
          name: 'subCategory',
          type: 'any'
        },
        "name": {
          name: 'name',
          type: 'string'
        },
        "about": {
          name: 'about',
          type: 'string'
        },
        "small_about": {
          name: 'small_about',
          type: 'string'
        },
        "pictures": {
          name: 'pictures',
          type: 'Array&lt;any&gt;'
        },
        "price": {
          name: 'price',
          type: 'number'
        },
        "dealer": {
          name: 'dealer',
          type: 'string'
        },
        "available": {
          name: 'available',
          type: 'number'
        },
      },
      relations: {
      }
    }
  }
}
