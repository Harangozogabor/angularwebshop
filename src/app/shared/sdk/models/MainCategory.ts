/* tslint:disable */

declare var Object: any;
export interface MainCategoryInterface {
  "id"?: any;
  "name"?: string;
}

export class MainCategory implements MainCategoryInterface {
  "id": any;
  "name": string;
  constructor(data?: MainCategoryInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `MainCategory`.
   */
  public static getModelName() {
    return "MainCategory";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of MainCategory for dynamic purposes.
  **/
  public static factory(data: MainCategoryInterface): MainCategory{
    return new MainCategory(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'MainCategory',
      plural: 'MainCategories',
      path: 'MainCategories',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "name": {
          name: 'name',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
