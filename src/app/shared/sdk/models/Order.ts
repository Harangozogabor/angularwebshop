/* tslint:disable */

declare var Object: any;
export interface OrderInterface {
  "id"?: any;
  "products"?: Array<any>;
  "user"?: any;
  "post_address"?: string;
  "billing_address"?: string;
  "status"?: string;
  "order_date"?: string;
  "expected_arrive_date"?: string;
  "company"?: string;
  "company_id"?: string;
  "company_tax_number"?: string;
}

export class Order implements OrderInterface {
  "id": any;
  "products": Array<any>;
  "user": any;
  "post_address": string;
  "billing_address": string;
  "status": string;
  "order_date": string;
  "expected_arrive_date": string;
  "company": string;
  "company_id": string;
  "company_tax_number": string;
  constructor(data?: OrderInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Order`.
   */
  public static getModelName() {
    return "Order";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Order for dynamic purposes.
  **/
  public static factory(data: OrderInterface): Order{
    return new Order(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Order',
      plural: 'Orders',
      path: 'Orders',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "products": {
          name: 'products',
          type: 'Array&lt;any&gt;'
        },
        "user": {
          name: 'user',
          type: 'any'
        },
        "post_address": {
          name: 'post_address',
          type: 'string'
        },
        "billing_address": {
          name: 'billing_address',
          type: 'string'
        },
        "status": {
          name: 'status',
          type: 'string'
        },
        "order_date": {
          name: 'order_date',
          type: 'string'
        },
        "expected_arrive_date": {
          name: 'expected_arrive_date',
          type: 'string'
        },
        "company": {
          name: 'company',
          type: 'string'
        },
        "company_id": {
          name: 'company_id',
          type: 'string'
        },
        "company_tax_number": {
          name: 'company_tax_number',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
