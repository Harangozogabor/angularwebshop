/* tslint:disable */

declare var Object: any;
export interface SubCategoryInterface {
  "id"?: any;
  "mainCategory"?: any;
  "name"?: string;
}

export class SubCategory implements SubCategoryInterface {
  "id": any;
  "mainCategory": any;
  "name": string;
  constructor(data?: SubCategoryInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `SubCategory`.
   */
  public static getModelName() {
    return "SubCategory";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of SubCategory for dynamic purposes.
  **/
  public static factory(data: SubCategoryInterface): SubCategory{
    return new SubCategory(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'SubCategory',
      plural: 'SubCategories',
      path: 'SubCategories',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "mainCategory": {
          name: 'mainCategory',
          type: 'any'
        },
        "name": {
          name: 'name',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
