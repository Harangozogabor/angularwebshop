/* tslint:disable */

declare var Object: any;
export interface OpinionInterface {
  "id"?: any;
  "product"?: any;
  "user"?: any;
  "rate"?: number;
  "about"?: string;
  "date"?: string;
}

export class Opinion implements OpinionInterface {
  "id": any;
  "product": any;
  "user": any;
  "rate": number;
  "about": string;
  "date": string;
  constructor(data?: OpinionInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Opinion`.
   */
  public static getModelName() {
    return "Opinion";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Opinion for dynamic purposes.
  **/
  public static factory(data: OpinionInterface): Opinion{
    return new Opinion(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Opinion',
      plural: 'Opinions',
      path: 'Opinions',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "product": {
          name: 'product',
          type: 'any'
        },
        "user": {
          name: 'user',
          type: 'any'
        },
        "rate": {
          name: 'rate',
          type: 'number'
        },
        "about": {
          name: 'about',
          type: 'string'
        },
        "date": {
          name: 'date',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
