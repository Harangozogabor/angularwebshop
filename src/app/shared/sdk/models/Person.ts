/* tslint:disable */

declare var Object: any;
export interface PersonInterface {
  "id"?: any;
  "firstName"?: string;
  "lastName"?: string;
  "phone"?: string;
  "email"?: string;
  "billingPostCode"?: string;
  "billingCity"?: string;
  "billingStreet"?: string;
  "shippingPostCode"?: string;
  "shippingCity"?: string;
  "shippingStreet"?: string;
  "companyName"?: string;
  "companyTax"?: string;
}

export class Person implements PersonInterface {
  "id": any;
  "firstName": string;
  "lastName": string;
  "phone": string;
  "email": string;
  "billingPostCode": string;
  "billingCity": string;
  "billingStreet": string;
  "shippingPostCode": string;
  "shippingCity": string;
  "shippingStreet": string;
  "companyName": string;
  "companyTax": string;
  constructor(data?: PersonInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Person`.
   */
  public static getModelName() {
    return "Person";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Person for dynamic purposes.
  **/
  public static factory(data: PersonInterface): Person{
    return new Person(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Person',
      plural: 'People',
      path: 'People',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "firstName": {
          name: 'firstName',
          type: 'string'
        },
        "lastName": {
          name: 'lastName',
          type: 'string'
        },
        "phone": {
          name: 'phone',
          type: 'string'
        },
        "email": {
          name: 'email',
          type: 'string'
        },
        "billingPostCode": {
          name: 'billingPostCode',
          type: 'string'
        },
        "billingCity": {
          name: 'billingCity',
          type: 'string'
        },
        "billingStreet": {
          name: 'billingStreet',
          type: 'string'
        },
        "shippingPostCode": {
          name: 'shippingPostCode',
          type: 'string'
        },
        "shippingCity": {
          name: 'shippingCity',
          type: 'string'
        },
        "shippingStreet": {
          name: 'shippingStreet',
          type: 'string'
        },
        "companyName": {
          name: 'companyName',
          type: 'string'
        },
        "companyTax": {
          name: 'companyTax',
          type: 'string'
        },
      },
      relations: {
      }
    }
  }
}
