/* tslint:disable */

declare var Object: any;
export interface CartInterface {
  "id"?: any;
  "products"?: Array<any>;
}

export class Cart implements CartInterface {
  "id": any;
  "products": Array<any>;
  constructor(data?: CartInterface) {
    Object.assign(this, data);
  }
  /**
   * The name of the model represented by this $resource,
   * i.e. `Cart`.
   */
  public static getModelName() {
    return "Cart";
  }
  /**
  * @method factory
  * @author Jonathan Casarrubias
  * @license MIT
  * This method creates an instance of Cart for dynamic purposes.
  **/
  public static factory(data: CartInterface): Cart{
    return new Cart(data);
  }
  /**
  * @method getModelDefinition
  * @author Julien Ledun
  * @license MIT
  * This method returns an object that represents some of the model
  * definitions.
  **/
  public static getModelDefinition() {
    return {
      name: 'Cart',
      plural: 'Carts',
      path: 'Carts',
      idName: 'id',
      properties: {
        "id": {
          name: 'id',
          type: 'any'
        },
        "products": {
          name: 'products',
          type: 'Array&lt;any&gt;'
        },
      },
      relations: {
      }
    }
  }
}
