import { Component } from '@angular/core';
import { environment } from '../environments/environment'
import { CookieService } from 'ngx-cookie-service';
import * as firebase from 'firebase/app';
import { SDKToken, LoopBackConfig } from './shared/sdk';
import { LoopBackAuth } from './shared/sdk/services/core/auth.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthService } from './services/auth.service';
import { TranslateService } from '@ngx-translate/core';
declare function require(name: string);
const uuidv1 = require('uuid/v1');
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';

  constructor(private cookie: CookieService, private auth: LoopBackAuth, private http: HttpClient, private translate: TranslateService
  ) {
    LoopBackConfig.setBaseURL(environment.loopbackUrl);
    LoopBackConfig.setApiVersion('api');
    if (this.cookie.get('cart') === '') {
      this.cookie.set('cart', uuidv1());
    }
    translate.setDefaultLang('hu');
    translate.use('hu');
  }

}





