import { Component, OnInit, Input, HostListener, ElementRef } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';
import { flatMap, switchMap } from 'rxjs/operators';
import { AngularFireAuth } from 'angularfire2/auth';
import { DataService } from '../services/data.service';
import { environment } from 'src/environments/environment';
import { Observable, of, from } from 'rxjs';

const URLDownload = environment.URLDownload;
@Component({
  selector: 'app-user-menu',
  templateUrl: './user-menu.component.html',
  styleUrls: ['./user-menu.component.scss']
})
export class UserMenuComponent implements OnInit {

  isOpen = false;
  userName = " ";
  userData;
  isLoggedIn = false;
  isAdmin = false;
  imgUrl:String;
  @Input() currentUser = null;
  @HostListener('document:click', ['$event', '$event.target'])
  onClick(event: MouseEvent, targetElement: HTMLElement) {
    if (!targetElement) {
      return;
    }

    const clickedInside = this.elementRef.nativeElement.contains(targetElement);
    if (!clickedInside) {
      this.isOpen = false;
    }
  }
  constructor(private elementRef: ElementRef, private _auth: AuthService, private router: Router, private afAuth: AngularFireAuth, private dataService:DataService) { }

  ngOnInit() {

    this._auth.cast.pipe(switchMap(data => {
      if (data) {
        this.userName = data.lastName + " " + data.firstName;
        this._auth.getUser() && this._auth.getUser().photoURL ? this.imgUrl=this._auth.getUser().photoURL :  this.imgUrl=null;
        this.isLoggedIn = this._auth.isLoggedIn();
      }else{
        this.imgUrl=null;
      }
      return from(this.dataService.getUserByEmail(this._auth.getUserEmail()));
    })).subscribe(data => {
      if ( data.length === 1) {
        if(!this.imgUrl){
          this.imgUrl = data[0].profileImage ? URLDownload + data[0].profileImage : null;
        }
      }
    });
    this._auth.castChangedUser.pipe(switchMap(()=>{
      this.isLoggedIn = this._auth.isLoggedIn();
      return this.afAuth.authState;
    })).subscribe(user=>{
      if(user){
        this.isAdmin=user.providerData[0].providerId==='phone';
      }
    });
  }

  logout() {
    sessionStorage.removeItem('email');
    localStorage.removeItem('email');
    this._auth.logout();
    this.router.navigateByUrl('/')
  }
  navigate() {
    this._auth.getUserData().then(data => {
      this.router.navigate(["/userDetails", data[0].id]);
    })

  }
}
