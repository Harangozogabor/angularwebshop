import { Component, OnInit, Input,Output,EventEmitter } from '@angular/core';
import { OrderService } from '../services/order.service';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent implements OnInit {
  @Input() stepper;
  @Output() finishedEvent = new EventEmitter<string>();
  isCompany = false;
  isTheSame = true;
  billing = {
    id: undefined,
    lastName: '',
    firstName: '',
    postCode: 0,
    city: '',
    street: '',
    phone: '',
    email: ''
  };
  shipping = {
    lastName: '',
    firstName: '',
    postCode: 0,
    city: '',
    street: '',
    phone: '',
    email: ''
  };
  company = {
    name: '',
    id: '',
    taxNumber: ''
  };
  filledProfile=false;
  constructor(private orderService: OrderService,public _auth:AuthService) { 
  }

  ngOnInit() {
     this._auth.castFilled.subscribe(data=>{
      this.filledProfile=data;
    });
  }
  ngAfterViewInit(){
    
  }

  send() {
    
    this.orderService.setProcurer({
      shipping: this.shipping,
      billing: this.isTheSame ? this.shipping : this.billing,
      company: this.isCompany ? this.company : ''
    });

  }

}
