import { Component, OnInit, ViewChild, Inject,ChangeDetectorRef } from '@angular/core';
import { MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { DataService } from '../services/data.service';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import {FormControl} from '@angular/forms';
@Component({
  selector: 'app-my-orders',
  templateUrl: './my-orders.component.html',
  styleUrls: ['./my-orders.component.css']
})
export class MyOrdersComponent implements OnInit {
  displayedColumns: string[] = ['edit', 'id',  'date', 'status'];
  dataSource: MatTableDataSource<any>;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  adminStuffs;
  constructor(public authService: AuthService, private _dataService: DataService, private _router: Router, public dialog: MatDialog,private changeDetectorRefs: ChangeDetectorRef) {
    this.refresh();
  }

  ngOnInit() {
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }


  openDialog(order): void {
    const dialogRef = this.dialog.open(ConfirmDialog2, {
      width: '250px',
      data: { order: order }
    }).afterClosed().subscribe(result => {
      this.refresh();
    });
  }

  refresh(){
    this._dataService.getOrdersByEmail(this.authService.getUserEmail()).subscribe(data => {
      for(let i =0; i<data.length;i++){
        data[i].username=data[i].user.lastName+' '+data[i].user.firstName;
      }
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      this.dataSource.sortingDataAccessor = (item, property) => {
        switch (property) {
           case 'date': return new Date(item.order_date);
           default: return item[property];
        }
      };
      this.changeDetectorRefs.detectChanges();
    })
  }


}

@Component({
  selector: 'confirm-dialog',
  templateUrl: 'confirmDialog.html',
})
export class ConfirmDialog2 {
  order;
  constructor(
    public dialogRef: MatDialogRef<ConfirmDialog2>, @Inject(MAT_DIALOG_DATA) public data, private dataService:DataService) {
      this.order=data.order;
    }

  onNoClick(): void {
    this.dialogRef.close();
  }
  onYesClick(): void {
    this.dialogRef.close();
    this.dataService.updateOrder(this.order.id,'Felhasználó által lemondott').then(()=>{
    });
  }
}