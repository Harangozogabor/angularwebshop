import { Component, OnInit, Input } from '@angular/core';
import { OrderService } from '../services/order.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-shipping',
  templateUrl: './shipping.component.html',
  styleUrls: ['./shipping.component.css']
})
export class ShippingComponent implements OnInit {
  @Input() stepper;
  shipping=0;
  payment=0;
  constructor(private orderService:OrderService, private router: Router) { }

  ngOnInit() {
    this.orderService.setPageState(2);
  }

  send(){
    if(this.shipping==0){
      this.orderService.setShipmentType('Normál szállítás a magyar Postával');
    }else{
      this.orderService.setShipmentType('Elsőbbségi szállítás a magyar Postával');
    }
    if(this.payment==0){
      this.orderService.setPayType('Előre utalás');
    }else{
      this.orderService.setPayType('Előre utalás');
    }
    this.stepper.next();
  }

}
