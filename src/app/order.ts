import { Product } from "./shared/sdk";
import { Person } from "./person";

export interface Order {
    id: String,
    products: Product[],
    user: Person,
    status: String,
    order_date: String,
    expected_arrive_date: string,
}
