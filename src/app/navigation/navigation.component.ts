import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { SidenavService } from '../services/sidenav.service';
import { MatSidenav } from '@angular/material';
import { DataService } from '../services/data.service';
import {FormControl} from '@angular/forms';
@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  showSearch = true;
  categories:any[];
  @ViewChild('sidenav') sidenav2: MatSidenav;
  myControl = new FormControl();
  options;
  constructor(private _dataService:DataService, private sidenavService: SidenavService,private cdRef:ChangeDetectorRef) { }

  ngOnInit() {
    
    this.categories= [];
    this._dataService.getMainCategories().subscribe(data=>{
      data.forEach(element => {
        this.categories.push(
          {
            id: element.id,
            name: element.name,
            clicked: false
          }
        )
      });
    })
    this.sidenavService.setSidenav(this.sidenav2);
  }

  width(type: string) {
    if (type === 'up' ) {
      document.getElementById('container').style.width = '100%';
    } else {
      if (!this.sidenav2.opened) {
        setTimeout(() => {
          document.getElementById('container').style.width = '0%';
        }, 300);
      }
    }
  }
  search(){
   //debounce
    this.options=this._dataService.search(this.myControl.value.toLowerCase());
  }


}
