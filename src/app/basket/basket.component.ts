import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';
import { Subscription } from 'rxjs';
import { environment } from 'src/environments/environment';
@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss']
})
export class BasketComponent implements OnInit {
  URLDownload = environment.URLDownload;
  private subscription: Subscription;
  public showMiniBasket = false;
  public cartlength = 0;
  sum;
  public basketContent;
  constructor(private cartService: CartService) {

  }

  ngOnInit() {
    this.subscription = this.cartService.cast
      .subscribe(item => {
        this.cartlength = item;
        this.initializer();
      });

  }
  mouseEnter(showMiniBasket) {
    this.showMiniBasket = showMiniBasket;
  }
  initializer() {
    this.cartService.getCartContent().then(data => {
      this.basketContent = data;
      if(this.basketContent[0]){
        for (let i = 0; i < this.basketContent[0].products.length; i++) {
          this.basketContent[0].products[i].amount = 0;
          console.log(this.basketContent[0].products[i].amount)
        }
        this.sum = 0;
        for (let i = 0; i < this.basketContent[0].products.length; i++) {
          this.sum += this.basketContent[0].products[i].price;
          this.basketIsConatin(this.basketContent[0].products[i]);
        }
      }
    })
  }

  basketIsConatin(product) {
    for (let i = 0; i < this.basketContent[0].products.length; i++) {
      if (product.id == this.basketContent[0].products[i].id) {
        this.basketContent[0].products[i].amount++;
        return;
      }
    }
    product.amount=1;
    this.basketContent.push({ product: product})
  }

}
