import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';
@Component({
  selector: 'app-order-navigation',
  templateUrl: './order-navigation.component.html',
  styleUrls: ['./order-navigation.component.css']
})
export class OrderNavigationComponent implements OnInit {
  constructor(private cartService: CartService) { }
  length = 0;
  finishedEvent;
  ngOnInit() {
    this.cartService.cast.subscribe(length=>{
      this.length= length;
    })

  }

  finisehed($event) {
    if($event===true){
      this.finishedEvent = true;
    }else if($event===false){
      this.finishedEvent = false;
    }
    
  }
}
