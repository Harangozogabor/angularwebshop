import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrderNavigationComponent } from './order-navigation.component';

describe('OrderNavigationComponent', () => {
  let component: OrderNavigationComponent;
  let fixture: ComponentFixture<OrderNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrderNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrderNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
