import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { AdminComponent } from './admin/admin.component';
import { AuthGuard } from './auth.guard';
import { StuffsComponent } from './stuffs/stuffs.component';
import { ProductComponent } from './product/product.component';
import { UploadComponent } from './upload/upload.component';
import { OrderComponent } from './order/order.component';
import { AddressComponent } from './address/address.component';
import { ShippingComponent } from './shipping/shipping.component';
import { SumComponent } from './sum/sum.component';
import { OrderNavigationComponent } from './order-navigation/order-navigation.component';
import { OrderService } from './services/order.service';
import { UserDetailsComponent } from './user-details/user-details.component';
import { MyOrdersComponent } from './my-orders/my-orders.component';
import { UpdateProductComponent } from './update-product/update-product.component';
import { PhoneLoginComponent } from './phone-login/phone-login.component';
import { AdminGuard } from './admin.guard';
import { UserGuard } from './user.guard';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'adminLogin',
    component: PhoneLoginComponent,
     canActivate: [AuthGuard]
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'updateProduct',
    component: UpdateProductComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'userDetails/:id',
    component: UserDetailsComponent
  },
  {
    path: 'upload',
    component: UploadComponent,
    canActivate: [AdminGuard]
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path: 'products',
    component: StuffsComponent
  },
  {
    path: 'myOrders',
    component: MyOrdersComponent,
    canActivate: [UserGuard]
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },

  {
    path: 'product/:id',
    component: ProductComponent
  },
  {
    path: 'order',
    component: OrderNavigationComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
