import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from '../services/data.service';
import { SidenavService } from '../services/sidenav.service';
import { Options } from 'ng5-slider';
import { environment } from '../../environments/environment'
import { NgxUiLoaderService } from 'ngx-ui-loader';
import { debounceTime, switchMap,distinctUntilChanged, map, flatMap } from 'rxjs/operators';
import { Subject, Observable,forkJoin } from 'rxjs';
import { HttpClient } from '@angular/common/http';
const URLDownload =environment.URLDownload;

@Component({
  selector: 'app-stuffs',
  templateUrl: './stuffs.component.html',
  styleUrls: ['./stuffs.component.scss']
})
export class StuffsComponent implements OnInit {
  constructor(private _dataService: DataService,private route: ActivatedRoute, private loader: NgxUiLoaderService, private Http:HttpClient) { }
  searchTerms = new Subject<Object>();
  stuffs;
  minValue=0;
  maxValue=0; 
  options: Options = {
    floor: 0,
    ceil: 100,
    step: 1,
    noSwitching: true,
    translate: (value: number): string => {
      return  value + ' Ft';
    }
  };
  ngOnInit() {

  
    this.searchTerms.pipe(debounceTime(100),distinctUntilChanged(),switchMap((obj:any) =>  this.getNewProducts(obj.min,obj.max))).subscribe(data=>{
      this.stuffs=data;
      for(let i=0; i < this.stuffs.length; i++){
        for(let j=0; j<this.stuffs[i].pictures.length;j++){
          this.stuffs[i].pictures[j]=this.stuffs[i].pictures[j];
        }
      }
    })
      
    
    this.loader.start();
    this.route.queryParams.pipe(switchMap(params=>{
      if(params.mainCategory){
        return  this._dataService.getProductsByCategory(params.mainCategory,params.subCategory);
      }else{
        return this._dataService.getProducts();
      }
    })).subscribe(data=>{
      this.stuffs=data;
      for(let i=0; i < this.stuffs.length; i++){
        this._dataService.getRatings(this.stuffs[i].id).subscribe(data=>{
          if(data.length==0){
            this.stuffs[i].rating=0;
          }else{
            let ratingNum=0;
            let ratingSum=0;
            data.forEach(rate => {
              ratingNum++;
              ratingSum+=rate.rate;
            });
            this.stuffs[i].rating=ratingSum/ratingNum;
          }
        })
        for(let j=0; j<this.stuffs[i].pictures.length;j++){
          this.stuffs[i].pictures[j]= this.stuffs[i].pictures[j];
          if(this.stuffs[i].price>this.options.ceil){
            this.maxValue = this.options.ceil = this.stuffs[i].price;
            
          }
        }
      }
      this.loader.stop();
    });

  }
  filterProducts(){
    this.searchTerms.next({min:this.minValue,max:this.maxValue});
  }

  getNewProducts(min,max){
    return this._dataService.getProductByPrice(min,max)
  }

  pageChanged(event){
    
  }



}
