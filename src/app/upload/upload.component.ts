import { Component, OnInit, Renderer2 } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { DataService } from '../services/data.service';
import { environment } from '../../environments/environment';
import { DomSanitizer } from '@angular/platform-browser';
import * as XLSX from 'xlsx';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { FileUploader, FileItem, FileLikeObject } from 'ng2-file-upload';
import { MainCategory, SubCategory, Product } from '../shared/sdk';




declare function require(name: string);
const uuidv1 = require('uuid/v1');
const URLUpload = environment.URLUpload;
const URLDownload = environment.URLDownload;

type AOA = any[][];
@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})


export class UploadComponent implements OnInit {
  public editor;
  public editorContent = `<h3>I am Example content</h3>`;
  public editorOptions = {
    placeholder: "insert content..."
  };
  generatedId: string;
  j = 0;
  i = 0;
  showImage = URLDownload + '3f8c98f0-dbc1-11e8-b60e-3db7f54787f7.png';
  public uploader: FileUploader = new FileUploader({
    url: URLUpload,
    isHTML5: true,
    allowedMimeType: ['image/png', 'image/jpg', 'image/jpeg', 'image/gif'],
    maxFileSize: 10 * 1024 * 1024 // 10 MB
  });
  uploadedImage: File;
  imagePreview;
  mainCategory: MainCategory = new MainCategory();
  subCategory: SubCategory = new SubCategory();
  product: Product = new Product();

  multipleProduct = [];
  selectedMainCategory;
  mainCategories = [];
  subCategories = [];
  productSubCategories = [];
  filenames = [];
  mainCategoryResult = {
    text: '',
    color: 'red'
  }
  subCategoryResult = {
    text: '',
    color: 'red'
  }
  productResult = {
    text: '',
    color: 'red'
  }
  multipleUploadResult = {
    text: '',
    color: 'red'
  }
  quill;
  fb = new FormBuilder();
  productForm = this.fb.group({
    id: new FormControl(),
    subCategory: new FormControl(null,Validators.required),
    name: new FormControl('', Validators.required),
    small_about: new FormControl(),
    price: new FormControl(0, [Validators.required,Validators.min(1)]),
    dealer: new FormControl('', Validators.required),
    available: new FormControl(0,[Validators.required,Validators.min(1)]),
  });
  data: AOA = [[1, 2], [3, 4]];
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  fileName: string = 'SheetJS.xlsx';
  constructor(private _dataService: DataService, public sanitizer: DomSanitizer, public imgmax: Ng2ImgMaxService) { }

  ngOnInit() {
    this.product.pictures = [];
    this.getMainCategories();
    this.getSubCategories();
  }
  getQuill(quill){
    this.quill=quill;
  }
  hasError(field:string){
    return this.productForm.controls[field].hasError('min');
  }


  onSubmitMainCategory() {
    if(!this.mainCategory.name){
      this.multipleUploadResult = {
        text: "Sikertelen feltöltés! A kategória neve nem maradhat üresen! ",
        color: "red",
      };
      return;
    }
    this._dataService.addMainCategory(this.mainCategory).subscribe(
      () => {
        this.getMainCategories();
        this.mainCategory = {
          id: '',
          name: ''
        };
        this.mainCategoryResult = {
          text: "Sikeres feltöltés! ",
          color: "green",
        };
      },
      () => {
        this.multipleUploadResult = {
          text: "Sikertelen feltöltés! ",
          color: "red",
        };
      }
    )
  }

  onSubmitSubCategory() {
    if(!this.subCategory.name || !this.subCategory.mainCategory){
      this.multipleUploadResult = {
        text: "Sikertelen feltöltés! A kategória neve nem maradhat üresen! ",
        color: "red",
      };
      return;
    }
    this._dataService.addSubCategory(this.subCategory).subscribe(
      () => {
        this.getSubCategories();
        this.subCategory = {
          id: '',
          name: '',
          mainCategory: ''
        };
        this.subCategoryResult = {
          text: "Sikeres feltöltés! ",
          color: "green",
        };
      },
      () => {
        this.subCategoryResult = {
          text: "Sikertelen feltöltés! ",
          color: "red",
        };
      }
    )
  }

  onSubmitProduct() {
    this.product.name = this.productForm.value.name;
    this.product.subCategory = this.productForm.value.subCategory;
    this.product.about = this.quill.container.firstChild.innerHTML
    this.product.small_about = this.productForm.value.small_about;
    this.product.price = this.productForm.value.price;
    this.product.dealer = this.productForm.value.dealer;
    this.product.available = this.productForm.value.available;
    this._dataService.addProduct(this.product).subscribe(
      () => {
        this.productForm.reset();
        this.product = new Product();
        this.product.pictures = [];
        this.productResult = {
          text: "Sikeres feltöltés! ",
          color: "green",
        };
      },
      () => {
        this.productResult = {
          text: "Sikertelen feltöltés! ",
          color: "red",
        };
      }
    )
  }

  getMainCategories() {
    this._dataService.getMainCategories().subscribe(data => {
      this.mainCategories = data;
    })
  }

  getSubCategories() {
    this._dataService.getSubCategories().subscribe(data => {
      this.subCategories = data;
    })
  }

  onChange($event) {
    this._dataService.getSubCategoriesByMainCategoryId($event.id).subscribe(data => {
      this.productSubCategories = data;
    })
  }
  onUploadChange(event, filecompress: number = 0.05) {
    const file = event.srcElement.files;
    this.imgmax.compressImage(file[this.j], filecompress).subscribe((compressedFile) => {
      const fileItem = new FileItem(this.uploader, compressedFile, this.uploader.options);
      const extension = fileItem.file.name.split('.');
      this.filenames.push(fileItem.file.name);
      this.generatedId = uuidv1() + '.' + extension[1];
      fileItem.file.name = this.generatedId;
      this.product.pictures.push(this.generatedId);
      this.uploader.queue.push(fileItem);
      this.uploader.queue[this.i].upload();
      this.uploader.onCompleteItem = async (item: any, response: any, status: any, headers: any) => {
        this.showImage = URLDownload + this.generatedId;
        this.i++;
        this.j++;
        if (this.j < file.length) {
          this.onUploadChange(event);
        } else {
          this.j = 0;
        }

      };
    }, error => {
      filecompress += 0.1;
      this.onUploadChange(event, filecompress);
    })
  }

  export(): void {
    /* generate worksheet */
    const ws: XLSX.WorkSheet = XLSX.utils.aoa_to_sheet(this.data);

    /* generate workbook and add the worksheet */
    const wb: XLSX.WorkBook = XLSX.utils.book_new();
    XLSX.utils.book_append_sheet(wb, ws, 'Sheet1');

    /* save to file */
    XLSX.writeFile(wb, this.fileName);
  }

  onFileChange(evt: any) {
    /* wire up file reader */
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, { type: 'binary' });

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];

      /* save data */
      this.data = <AOA>(XLSX.utils.sheet_to_json(ws, { header: 1 }));
    };
    reader.readAsBinaryString(target.files[0]);
  }

  formatData() {
    let product = {
      id: '',
      name: '',
      subCategory: '',
      about: '',
      pictures: [],
      price: 0,
      dealer: '',
      available: 0,
      small_about: ''
    };

    for (let i = 0; i < this.data.length; i++) {
      product.name = this.data[i][0];
      product.subCategory = this.findSubcategory(this.data[i][2]);
      product.about = this.data[i][3];
      product.price = this.data[i][4];
      product.available = this.data[i][5];
      product.dealer = this.data[i][6];
      product.small_about = this.data[i][7];
      this._dataService.addProduct(product).subscribe(() => {
        this.multipleUploadResult = {
          text: "Sikeres feltöltés! ",
          color: "green",
        };
      },
        () => {
          this.multipleUploadResult = {
            text: "Sikertelen feltöltés! ",
            color: "red",
          };
        })
    }
  }

  findSubcategory(name: string) {
    for (let i = 0; i < this.subCategories.length; i++) {
      if (this.subCategories[i].name == name) {
        return this.subCategories[i];
      }
    }
    return "";
  }

  setFocus($event) {
    $event.focus();
  }
}
