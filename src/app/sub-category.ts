import { MainCategory } from "./shared/sdk";

export interface SubCategory {
    id: String,
    mainCategory: MainCategory
    name: String
}
