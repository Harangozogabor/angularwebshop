import { Product } from "./product";

export interface Opinion {
    id: String,
    product: Product,
    user: Product,
    rate: Number,
    about: String,
    date: String
}
