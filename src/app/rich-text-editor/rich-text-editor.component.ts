import { Component, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import * as QuillNamespace from 'quill';
let Quill: any = QuillNamespace;
import ImageResize from 'quill-image-resize-module';
import VideoResize from 'quill-video-resize-module';
import { ImageDrop } from 'quill-image-drop-module';
import ImageUploader from "../../../node_modules/quill-image-uploader/src/quill.imageUploader";
import { environment } from 'src/environments/environment';
import { Ng2ImgMaxService } from 'ng2-img-max';
import { FileUploader, FileItem, FileLikeObject } from 'ng2-file-upload';
import { DataService } from '../services/data.service';
import { DomSanitizer } from '@angular/platform-browser';
declare function require(name: string);
const uuidv1 = require('uuid/v1');
const URLUpload = environment.URLUpload;
const URLDownload = environment.URLDownload;
@Component({
  selector: 'app-rich-text-editor',
  templateUrl: './rich-text-editor.component.html',
  styleUrls: ['./rich-text-editor.component.css']
})
export class RichTextEditorComponent implements OnInit {
  text1: any;
  generatedId: string;
  i = 0;
  showImage = URLDownload + '3f8c98f0-dbc1-11e8-b60e-3db7f54787f7.png';
  public uploader: FileUploader = new FileUploader({
    url: URLUpload,
    isHTML5: true,
    allowedMimeType: ['image/png', 'image/jpg', 'image/jpeg', 'image/gif'],
    maxFileSize: 10 * 1024 * 1024 // 10 MB
  });
  uploadedImage: File;
  @Output() quill_emitter: EventEmitter<any> = new EventEmitter();





  constructor(private _dataService: DataService, public sanitizer: DomSanitizer, public imgmax: Ng2ImgMaxService) {

  }

  ngOnInit() {
    var Size = Quill.import('attributors/style/size');
    Quill.register(Size, true);
    Quill.register('modules/imageResize', ImageResize);
    Quill.register('modules/imageUploader', ImageUploader);
    Quill.register('modules/VideoResize', VideoResize);
    Quill.register('modules/imageDrop', ImageDrop);
    var Image = Quill.import('formats/image');
    Image.className = 'img-fluid';

    Quill.register(Image, true);

    var SizeStyle = Quill.import('attributors/style/size');

    Quill.register(SizeStyle, true);

    var fontSizeStyle = Quill.import('attributors/style/size');
    fontSizeStyle.whitelist = ['20px', '22px', '24px', '26px', '28px', '30px', '32px', '34px', '36px', '38px', '40px', '42px', '44px', '46px', '48px'];
    Quill.register(fontSizeStyle, true);

    var toolbarOptions = [
      ['bold', 'italic', 'underline', 'strike'],
      [{ 'size': [false, '20px', '22px', '24px', '26px', '28px', '30px', '32px', '34px', '36px', '38px', '40px', '42px', '44px', '46px', '48px'] }],
      // toggled buttons
      ['blockquote', 'code-block'],

      [{ 'header': 1 }, { 'header': 2 }],               // custom button values
      [{ 'list': 'ordered' }, { 'list': 'bullet' }],
      [{ 'script': 'sub' }, { 'script': 'super' }],      // superscript/subscript
      [{ 'indent': '-1' }, { 'indent': '+1' }],          // outdent/indent
      [{ 'direction': 'rtl' }],                         // text direction

      [{ 'header': [1, 2, 3, 4, 5, 6, false] }],

      [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
      [{ 'font': [] }],
      [{ 'align': [] }],

      ['clean', 'link', 'image', 'video']
    ];

    var quill = new Quill('#editor1', {
      modules: {
        toolbar: 
        {
          container: toolbarOptions,
        },
        imageResize: {
          displaySize: true
        },
        VideoResize: {
          displaySize: true
        },
        imageDrop: true,
        imageUploader: {
          upload: file => {

            return new Promise((resolve, reject) => {
              const fileItem = new FileItem(this.uploader, file, this.uploader.options);
              const extension = fileItem.file.name.split('.');
              this.generatedId = uuidv1() + '.' + extension[1];
              fileItem.file.name = this.generatedId;
              this.uploader.queue.push(fileItem);
              this.uploader.queue[this.i].upload();
              this.uploader.onCompleteItem = async (item: any, response: any, status: any, headers: any) => {
                this.showImage = URLDownload + this.generatedId;
                resolve(
                  this.showImage
                );
                this.i++;
              };

            });
          }
        }

      },
      theme: 'snow',
      placeholder: 'Start typing....',
    });
    this.quill_emitter.emit(quill);
  }
  printText() {

  }

}
