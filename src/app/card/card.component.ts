import { Component, OnInit, Input } from '@angular/core';
import { CartService } from '../services/cart.service';
import {environment} from '../../environments/environment'



@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {
  URLDownload=environment.URLDownload;
  @Input() product;
  constructor(private cartService:CartService) { }

  ngOnInit() {
  }

  //save cart content to the database, make a cookie to identify cart content
  addToCart(){
    this.cartService.addToCart(this.product);
  }
}
