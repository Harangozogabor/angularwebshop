import { Injectable } from '@angular/core';
import { CanActivate, Router} from '@angular/router';
import { Observable } from 'rxjs';
import { AngularFireAuth } from 'angularfire2/auth';
import { map,first } from 'rxjs/operators';
import { AuthService } from './services/auth.service';
@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router,private afAuth: AngularFireAuth) {}

  canActivate() {
   return this.afAuth.authState.pipe(
      map(user => user.providerData[0].providerId==='phone'),
      first()
      )
  }
}
;