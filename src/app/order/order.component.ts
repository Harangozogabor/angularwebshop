import { Component, OnInit } from '@angular/core';
import { CartService } from '../services/cart.service';
import { OrderService } from '../services/order.service';
import { AuthService } from '../services/auth.service';
import { environment } from 'src/environments/environment';


@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.scss']
})

export class OrderComponent implements OnInit {
  filledProfile;
  basketContent = [

  ];
  sum = 0;
  URLDownload = environment.URLDownload;
  constructor(private cartService: CartService, private orderService: OrderService, private authService: AuthService) { }

  ngOnInit() {
    this.filledProfile = this.authService.filledProfile;
    this.orderService.setPageState(0);
    this.cartService.getCartContent().then(data => {
      if (data.length != 0) {
        for (let i = 0; i < data[0].products.length; i++) {
          this.sum += data[0].products[i].price;
          this.basketIsConatin(data[0].products[i]);
        }
      }
    })
  }

  basketIsConatin(product) {
    for (let i = 0; i < this.basketContent.length; i++) {
      if (product.id == this.basketContent[i].product.id) {
        this.basketContent[i].amount++;
        return;
      }
    }
    this.basketContent.push({ product: product, amount: 1 })
  }

  plus(product) {
    this.sum += product.product.price;
    product.amount++;
    this.cartService.addToCart(product.product).then(
      ()=>{
      },error=>{
        console.error(error)
      }
    );
  }

  minus(product) {
    if (product.amount > 1) {
      this.sum -= product.product.price;
      product.amount--;
      this.cartService.removeFromCart(product.product).then(
        ()=>{
        },error=>{
          console.error(error)
        }
      );
    }
  }
  remove(product) {
    for (let i = 0; i < this.basketContent.length; i++) {
      if (this.basketContent[i].product.id == product.product.id) {
        this.sum -= product.product.price * product.amount;
        this.basketContent.splice(i, 1);
      }
    }
    this.cartService.removeAllFromCart(product.product).then();
  }


}
